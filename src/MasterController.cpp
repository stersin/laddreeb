#include "MasterController.h"
#include "Config.h"

#define LADDDEBUG_PREFIX "DEBUG:MasterCtrl"
#define LADDDEBUG_STATES 0
#define LADDDEBUG_COMMANDS 1

MasterController::MasterController()
    : 
      _state(State::NONE),
      _subState(SubState::NONE_NONE),
      _serialController(Config::SERIAL_BAUDRATE),
      _emergSwitch("emergSwitch", Config::emergSwitchPin),
      _processSwitch("processSwitch", Config::processSwitchPin),
      _process2Switch("process2Switch", Config::process2SwitchPin),
      _dBottomSwitch("dBottomSwitch", Config::dBottomSwitchPin, true, true),
      _dTopSwitch("dTopSwitch", Config::dTopSwitchPin, true, true),
      _dAxis("dAxis", Config::dDirPin, Config::dStepPin, Config::dSpeed, Config::dReverseDir, &_dBottomSwitch, &_dTopSwitch),
      _dDistributeBeerRelay(Config::dDistributeBeerRelayPin, Config::dDistributeBeerRelayInverted),
      _rAxis("rAxis", Config::rDirPin, Config::rStepPin, Config::rSpeed, Config::rReverseDir),
      _zBottomSwitch("zBottomSwitch", Config::zBottomSwitchPin, true, true),
      _zTopSwitch("zTopSwitch", Config::zTopSwitchPin, true, true),
      _zAxis("zAxis", Config::zDirPin, Config::zStepPin, Config::zSpeed, Config::zReverseDir, &_zBottomSwitch, &_zTopSwitch),
      _yBottomSwitch("yBottomSwitch", Config::yBottomSwitchPin, true, true),
      _yTopSwitch("yTopSwitch", Config::yTopSwitchPin, true, true),
      _yAxis("yAxis", Config::yDirPin, Config::yStepPin, Config::ySpeed, Config::yReverseDir, &_yBottomSwitch, &_yTopSwitch),
      _tRevSensor("tRevSensor", Config::tRevSensor, true, false),
      _tAxis("tAxis", Config::tDirPin, Config::tStepPin, Config::tSpeed, Config::tReverseDir, &_tRevSensor),
      _grip1Relay(Config::grip1RelayPin, Config::grip1RelayInverted),
      _emptyPileSwitch("emptyPileSwitch", Config::emptyPileSwitchPin, true, true),
      _emptyEntrySwitch("emptyEntrySwitch", Config::emptyEntrySwitchPin, false, true),
      _rotatePileSwitch("rotatePileSwitch", Config::rotatePileSwitchPin),
      _inputNewLineEventHandler((LaddLib::Eventable::EventHandler::Callback)&onInputNewLineCallback, this)
{
}

void MasterController::onInit()
{
    _serialController.init();

    _serialController
        .registerEventHandler(LaddLib::SerialController::NewLineEvent::TYPE, _inputNewLineEventHandler);

    _emergSwitch.init();
    _processSwitch.init();
    _process2Switch.init();
    _dBottomSwitch.init();
    _dTopSwitch.init();
    _dDistributeBeerRelay.init();
    _tRevSensor.init();
    _zBottomSwitch.init();
    _zTopSwitch.init();
    _yBottomSwitch.init();
    _yTopSwitch.init();
    _grip1Relay.init();
    _emptyPileSwitch.init();
    _emptyEntrySwitch.init();
    _rotatePileSwitch.init();

    _dAxis.init();
    _rAxis.init();
    _zAxis.init();
    _yAxis.init();
    _tAxis.init();

    pinMode(Config::ultrasonic1TriggerPin, OUTPUT);
    digitalWrite(Config::ultrasonic1TriggerPin, LOW);
    pinMode(Config::ultrasonic1EchoPin, INPUT);

    pinMode(Config::ultrasonic2TriggerPin, OUTPUT);
    digitalWrite(Config::ultrasonic2TriggerPin, LOW);
    pinMode(Config::ultrasonic2EchoPin, INPUT);

    pinMode(Config::ultrasonic3TriggerPin, OUTPUT);
    digitalWrite(Config::ultrasonic3TriggerPin, LOW);
    pinMode(Config::ultrasonic3EchoPin, INPUT);
}

void MasterController::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
    _serialController.update();

    _emergSwitch.update();
    _processSwitch.update();
    _process2Switch.update();
    _dBottomSwitch.update();
    _dTopSwitch.update();
    _tRevSensor.update();
    _zBottomSwitch.update();
    _zTopSwitch.update();
    _yBottomSwitch.update();
    _yTopSwitch.update();
    _emptyPileSwitch.update();
    _emptyEntrySwitch.update();
    _rotatePileSwitch.update();

    _dDistributeBeerRelay.update();

    _dAxis.update();
    _rAxis.update();
    _zAxis.update();
    _yAxis.update();
    _tAxis.update();

    // Serial.println(_getUltrasonic3Measure());
    // Serial.println(_getUltrasonic3Measure());
    // delayMicroseconds(10000);

    if (_emergSwitch.stateChangedToReleased())
    {
        if (_state != State::ERROR)
        {
            Serial.println("To State::ERROR");
            _state = State::ERROR;

            _dAxis.emerg();
            _rAxis.emerg();
            _zAxis.emerg();
            _yAxis.emerg();
            _tAxis.emerg();
        }
        else
        {
            Serial.println("To State::INIT");
            _state = State::INIT;
            Serial.println("To SubState::INIT_0_INIT");
            _subState = SubState::INIT_0_INIT;
        }
    }

    if (_processSwitch.stateChangedToReleased())
    {
        /*Serial.println("To State::TEST");
        _subState = SubState::TEST_INIT_1;
        _state = State::TEST;
        return;*/

        if (_state == State::NONE)
        {
            Serial.println("To State::INIT");
            _state = State::INIT;

            Serial.println("To SubState::INIT_0_INIT");
            _subState = SubState::INIT_0_INIT;
        }
        else if (_state == State::IDLE)
        {
            _distributorPos = 0;
            _numToServe = 1;
            _remainingToServe = 1;
            _qtyToDistribute = 25;

            _state = State::PICK_GLASS_RETURN;
            _subState = SubState::PICK_GLASS_RETURN_0_INIT;
        }
    }

    if (_process2Switch.stateChangedToReleased())
    {
        if (_state == State::IDLE)
        {
            Serial.println("To State::PICK_GLASSES_PILE");
            _state = State::PICK_GLASSES_PILE;

            Serial.println("To SubState::PICK_GLASSES_PILE_0_INIT");
            _subState = SubState::PICK_GLASSES_PILE_0_INIT;
        }
        else
        {
            Serial.println("_state != State::IDLE");
        }
    }

    if (_rotatePileSwitch.stateChangedToReleased())
    {
        if (_state == State::IDLE)
        {
            Serial.println("To State::ROTATE_PILE");
            _state = State::ROTATE_GLASSES_PILE;

            Serial.println("To SubState::ROTATE_PILE_0_INIT");
            _subState = SubState::ROTATE_GLASSES_PILE_0_INIT;
        }
    }

    if (this->_state == State::IDLE)
    {
        if(_remainingToServe == 0 && _emptyPileSwitch.isPushed()) {
            Serial.println("To State::PICK_GLASSES_PILE");
            _state = State::PICK_GLASSES_PILE;

            Serial.println("To SubState::PICK_GLASSES_PILE_0_INIT");
            _subState = SubState::PICK_GLASSES_PILE_0_INIT;
        } 
        else if (_remainingToServe > 0)
        {
            Serial.println("To State::PICK_GLASS");
            _state = State::PICK_GLASS;

            Serial.println("To SubState::PICK_GLASS_0_INIT");
            _subState = SubState::PICK_GLASS_0_INIT;
        }
    }
    else if (this->_state == State::INIT)
    {
        switch (_subState)
        {
        case SubState::INIT_0_INIT:
            _grip1Relay.deactivate();
            if (_emptyEntrySwitch.isPushed())
            {
                _zAxis.reset();

                Serial.println("To SubState::INIT_1_ZAXIS_TO_MIN");
                _subState = SubState::INIT_1_ZAXIS_TO_MIN;
            }
            break;

        case SubState::INIT_1_ZAXIS_TO_MIN:
            if (_zAxis.isReady() && _zAxis.state() == LaddLib::Component::AxisComponent::IS_DOWN)
            {
                _grip1Relay.deactivate();

                _dAxis.reset();

                Serial.println("To SubState::INIT_2_DAXIS_TO_MIN");
                _subState = SubState::INIT_2_DAXIS_TO_MIN;
            }
            break;

        case SubState::INIT_2_DAXIS_TO_MIN:
            if (_dAxis.isReady() && _dAxis.state() == LaddLib::Component::AxisComponent::IS_DOWN)
            {
                _grip1Relay.deactivate();

                if (_getUltrasonic2Measure() > Config::ultrasonic2DetectionDistance || _yAxis.state() != LaddLib::Component::AxisComponent::IS_DOWN)
                {
                    _dAxis.moveNumRev(Config::dDown1DestRev);

                    Serial.println("To SubState::INIT_2_DAXIS_TO_GRIP_POS");
                    _subState = SubState::INIT_2_DAXIS_TO_GRIP_POS;
                }
                else
                {
                    _yAxis.reset();

                    Serial.println("To SubState::INIT_ALT_2_YAXIS_TO_MIN");
                    _subState = SubState::INIT_ALT_2_YAXIS_TO_MIN;
                }
            }

            break;

        case SubState::INIT_2_DAXIS_TO_GRIP_POS:
            if (_dAxis.state() == LaddLib::Component::AxisComponent::IS_BETWEEN)
            {
                _grip1Relay.activate();
                delay(1000);
                _dAxis.goUp();

                Serial.println("TO SubState::INIT_2_DAXIS_TO_MAX");
                _subState = SubState::INIT_2_DAXIS_TO_MAX;
            }

            break;

        case SubState::INIT_2_DAXIS_TO_MAX:
            if (_dAxis.state() == LaddLib::Component::AxisComponent::IS_UP)
            {
                _yAxis.reset();

                Serial.println("To SubState::INIT_3_YAXIS_TO_MIN");
                _subState = SubState::INIT_3_YAXIS_TO_MIN;
            }

            break;

        case SubState::INIT_3_YAXIS_TO_MIN:
            if (_yAxis.isReady() && _yAxis.state() == LaddLib::Component::AxisComponent::IS_DOWN)
            {
                if (!_emptyPileSwitch.isPushed())
                {
                    _rAxis.reset();
                    _rAxis.moveNumRev(Config::rQuarterNumRev);

                    Serial.println("To SubState::INIT_4_RAXIS_TURN");
                    _subState = SubState::INIT_4_RAXIS_TURN;
                }
                else
                {
                    _tAxis.reset();
                    Serial.println("To SubState::INIT_6_TAXIS_TO_MAX");
                    _subState = SubState::INIT_6_TAXIS_TO_MAX;
                }
            }

            break;

        case SubState::INIT_4_RAXIS_TURN:
            if (_rAxis.state() == LaddLib::Component::AxisComponentTest::IS_BETWEEN)
            {
                _dAxis.goDown();

                Serial.println("To SubState::INIT_5_DAXIS_TO_MIN");
                _subState = SubState::INIT_5_DAXIS_TO_MIN;
            }

            break;

        case SubState::INIT_5_DAXIS_TO_MIN:
            if (_dAxis.state() == LaddLib::Component::AxisComponent::IS_DOWN)
            {
                if (_getUltrasonic2Measure() < Config::ultrasonic2DetectionDistance)
                {
                    _tAxis.reset();
                    Serial.println("To SubState::INIT_6_TAXIS_TO_MAX");
                    _subState = SubState::INIT_6_TAXIS_TO_MAX;
                }
                else
                {
                    Serial.println("To SubState::INIT_2_DAXIS_TO_MIN");
                    _subState = SubState::INIT_2_DAXIS_TO_MIN;
                }
            }

            break;

        case INIT_ALT_2_YAXIS_TO_MIN:
            if (_yAxis.isReady() && _yAxis.state() == LaddLib::Component::AxisComponent::IS_DOWN)
            {
                _tAxis.reset();
                Serial.println("To SubState::INIT_6_TAXIS_TO_MAX");
                _subState = SubState::INIT_6_TAXIS_TO_MAX;
            }
            break;

        case SubState::INIT_6_TAXIS_TO_MAX:
            if (_tAxis.isReady())
            {
                _currentGlassesPilesIndex = 0;
                Serial.println("To SubState::INIT_7_OPEN_GRIP1");
                _subState = SubState::INIT_7_OPEN_GRIP1;
            }

            break;

        case SubState::INIT_7_OPEN_GRIP1:
            _grip1Relay.deactivate();

            if (!_emptyPileSwitch.isPushed())
            {
                Serial.println("To State::IDLE");
                _subState = SubState::NONE_NONE;
                _state = State::IDLE;
            }
            else
            {
                Serial.println("To State::PICK_GLASSES_PILE");
                _subState = SubState::PICK_GLASSES_PILE_0_INIT;
                _state = State::PICK_GLASSES_PILE;
            }

            break;

        default:
            break;
        }
    }
    else if (_state == State::PICK_GLASS)
    {
        switch (_subState)
        {
        case SubState::PICK_GLASS_0_INIT:
            if (_emptyEntrySwitch.isPushed())
            {
                _yAxis.goDown();

                Serial.println("TO SubState::PICK_GLASS_1_DISTRIBUTE_BEER");
                _subState = SubState::PICK_GLASS_1_DISTRIBUTE_BEER;
            }
            break;

        case SubState::PICK_GLASS_1_DISTRIBUTE_BEER:
            _dDistributeBeerRelay.activateWithDelay(Config::dDistributeBeer1DelayMs * _qtyToDistribute / 50);

            Serial.println("To SubState::PICK_GLASS_1_DAXIS_TO_GRIP_POS");
            _subState = SubState::PICK_GLASS_1_DAXIS_TO_GRIP_POS;
            break;

        case SubState::PICK_GLASS_1_DAXIS_TO_GRIP_POS:
            if (_yAxis.state() == LaddLib::Component::AxisComponent::IS_DOWN && _dDistributeBeerRelay.getState() == LaddLib::Component::DigitalOutputComponent::State::DEACTIVATED)
            {
                _dAxis.moveNumRev(Config::dDown1DestRev);

                Serial.println("TO SubState::PICK_GLASS_2_CLOSE_GRIP1");
                _subState = SubState::PICK_GLASS_2_CLOSE_GRIP1;
            }

            break;

        case SubState::PICK_GLASS_2_CLOSE_GRIP1:
            if (_dAxis.state() == LaddLib::Component::AxisComponent::IS_BETWEEN)
            {
                _grip1Relay.activate();

                Serial.println("To SubState::PICK_GLASS_3_DISTRIBUTE_BEER");
                _subState = SubState::PICK_GLASS_3_DISTRIBUTE_BEER;
            }

            break;

        case SubState::PICK_GLASS_3_DISTRIBUTE_BEER:
            if (_dDistributeBeerRelay.getState() == LaddLib::Component::DigitalOutputComponent::State::DEACTIVATED)
            {
                _dAxis.goUp(Config::dDistributeBeerAxisSpeedRatio);
                _dDistributeBeerRelay.activateWithDelay(Config::dDistributeBeer2DelayMs);

                Serial.println("To SubState::PICK_GLASS_4_DAXIS_TO_MAX");
                _subState = SubState::PICK_GLASS_4_DAXIS_TO_MAX;
            }
            break;

        case SubState::PICK_GLASS_4_DAXIS_TO_MAX:
            if (_dAxis.state() == LaddLib::Component::AxisComponent::IS_UP)
            {
                _rAxis.moveNumRev(Config::rQuarterNumRev);
                Serial.println("To SubState::PICK_GLASS_5_RAXIS_TURN");
                _subState = SubState::PICK_GLASS_5_RAXIS_TURN;
            }
            break;

        case SubState::PICK_GLASS_5_RAXIS_TURN:
            if (_rAxis.state() == LaddLib::Component::AxisComponentTest::IS_BETWEEN)
            {
                if (!_emptyPileSwitch.isPushed())
                {
                    _dAxis.goDown();

                    Serial.println("To SubState::PICK_GLASS_6_DAXIS_TO_MIN");
                    _subState = SubState::PICK_GLASS_6_DAXIS_TO_MIN;
                }
                else
                {
                    Serial.println("To SubState::PICK_GLASS_7_OPEN_GRIP1");
                    _subState = SubState::PICK_GLASS_7_OPEN_GRIP1;
                }
            }
            break;

        case SubState::PICK_GLASS_6_DAXIS_TO_MIN:
            if (_dAxis.state() == LaddLib::Component::AxisComponent::IS_DOWN)
            {
                Serial.println("To SubState::PICK_GLASS_7_OPEN_GRIP1");
                _subState = SubState::PICK_GLASS_7_OPEN_GRIP1;
            }
            break;

        case SubState::PICK_GLASS_7_OPEN_GRIP1:
            _grip1Relay.deactivate();

            _remainingToServe--;

            Serial.println("To SubState::PICK_GLASS_8_WAIT_FOR_GLASS_TO_BE_TAKEN");
            _subState = SubState::PICK_GLASS_8_WAIT_FOR_GLASS_TO_BE_TAKEN;

            break;

        case SubState::PICK_GLASS_8_WAIT_FOR_GLASS_TO_BE_TAKEN:
            if(_emptyEntrySwitch.isPushed())
            {
                Serial.println("To State::IDLE");
                _subState = SubState::NONE_NONE;
                _state = State::IDLE;
                
                break;
            }


        default:
            break;
        }
    }
    else if (_state == State::PICK_GLASS_RETURN)
    {
        switch (_subState)
        {
        case SubState::PICK_GLASS_RETURN_0_INIT:
            if (!_emptyEntrySwitch.isPushed())
            {
                _grip1Relay.activate();
                Serial.println("TO SubState::PICK_GLASS_RETURN_1_CLOSE_GRIP1");
                _subState = SubState::PICK_GLASS_RETURN_1_CLOSE_GRIP1;
            }
            break;

        case SubState::PICK_GLASS_RETURN_1_CLOSE_GRIP1:
            _dAxis.goUp();

            Serial.println("TO SubState::PICK_GLASS_RETURN_2_DAXIS_TO_MAX");
            _subState = SubState::PICK_GLASS_RETURN_2_DAXIS_TO_MAX;        

            break;

        case SubState::PICK_GLASS_RETURN_2_DAXIS_TO_MAX:
            if (_dAxis.state() == LaddLib::Component::AxisComponent::IS_UP)
            {
                _rAxis.moveNumRev(-Config::rQuarterNumRev);

                Serial.println("TO SubState::PICK_GLASS_RETURN_3_RAXIS_TURN_REVERSE");
                _subState = SubState::PICK_GLASS_RETURN_3_RAXIS_TURN_REVERSE;
            }
            break;

        case SubState::PICK_GLASS_RETURN_3_RAXIS_TURN_REVERSE:
            if (_rAxis.state() == LaddLib::Component::AxisComponentTest::State::IS_BETWEEN)
            {
                _dAxis.moveToRev(Config::dDown1DestRev);

                Serial.println("TO SubState::PICK_GLASS_RETURN_4_DAXIS_TO_GRIP_POS");
                _subState = SubState::PICK_GLASS_RETURN_4_DAXIS_TO_GRIP_POS;
            }
            break;

        case SubState::PICK_GLASS_RETURN_4_DAXIS_TO_GRIP_POS:
            if (_dAxis.state() == LaddLib::Component::AxisComponent::IS_BETWEEN)
            {
                _grip1Relay.deactivate();

                Serial.println("TO SubState::PICK_GLASS_RETURN_5_OPEN_GRIP1");
                _subState = SubState::PICK_GLASS_RETURN_5_OPEN_GRIP1;
            }

            break;

        case SubState::PICK_GLASS_RETURN_5_OPEN_GRIP1:
            _dAxis.goDown();

            Serial.println("TO SubState::PICK_GLASS_RETURN_6_DAXIS_TO_MIN");
            _subState = SubState::PICK_GLASS_RETURN_6_DAXIS_TO_MIN;
    
            break;

        case SubState::PICK_GLASS_RETURN_6_DAXIS_TO_MIN:
            if (_dAxis.state() == LaddLib::Component::AxisComponent::IS_DOWN)
            {
                _state = State::PICK_GLASS;
                _subState = SubState::PICK_GLASS_0_INIT;
            }

            break;

        default:
            break;
        }
    }
    else if (_state == State::PICK_GLASSES_PILE)
    {
        switch (_subState)
        {
        case SubState::PICK_GLASSES_PILE_0_INIT:
        {
            if (!_emptyPileSwitch.isPushed())
            {
                Serial.println("Cannot process State::PICKUP_GLASSES_PILE, empty pile switch is not active");
                Serial.println("To State::IDLE");
                _state = State::IDLE;
                _subState = SubState::NONE_NONE;

                return;
            }

            for (int i = 0; i < NUM_GLASSES_PILES; i++)
            {
                _glassesPiles[i] = false;
            }

            Serial.println("To SubState::PICK_GLASSES_PILE_1_OPEN_GRIP1");
            _subState = SubState::PICK_GLASSES_PILE_1_OPEN_GRIP1;
        }
        break;

        case SubState::PICK_GLASSES_PILE_1_OPEN_GRIP1:
            _grip1Relay.deactivate();
            _dAxis.goUp();

            Serial.println("To SubState::PICK_GLASSES_PILE_2_DAXIS_TO_MAX");
            _subState = SubState::PICK_GLASSES_PILE_2_DAXIS_TO_MAX;

            break;

        case SubState::PICK_GLASSES_PILE_2_DAXIS_TO_MAX:
            if (_dAxis.state() == LaddLib::Component::AxisComponent::IS_UP)
            {
                _zAxis.goDown();

                Serial.println("To SubState::PICK_GLASSES_PILE_3_ZAXIS_TO_MIN");
                _subState = SubState::PICK_GLASSES_PILE_3_ZAXIS_TO_MIN;
            }
            break;

        case SubState::PICK_GLASSES_PILE_3_ZAXIS_TO_MIN:
            if (_zAxis.state() == LaddLib::Component::AxisComponent::IS_DOWN)
            {
                _yAxis.goUp();

                Serial.println("To SubState::PICK_GLASSES_PILE_4_YAXIS_TO_MAX");
                _subState = SubState::PICK_GLASSES_PILE_4_YAXIS_TO_MAX;
            }
            break;

        case SubState::PICK_GLASSES_PILE_4_YAXIS_TO_MAX:
            if (_yAxis.state() == LaddLib::Component::AxisComponent::IS_UP)
            {
                Serial.println("To SubState::PICK_GLASSES_PILE_5_FIND_NEXT_GLASSES_PILE");
                _subState = SubState::PICK_GLASSES_PILE_5_FIND_NEXT_GLASSES_PILE;
            }
            break;

        case SubState::PICK_GLASSES_PILE_5_FIND_NEXT_GLASSES_PILE:
        {
            uint32_t distanceMm = _getUltrasonic1Measure();

            int toCheckIndex = _currentGlassesPilesIndex == 0 ? NUM_GLASSES_PILES - 1 : _currentGlassesPilesIndex - 1;

            _glassesPiles[toCheckIndex] = distanceMm < Config::ultrasonic1DetectionDistance;

            _tAxis.rotateNumRev(-1800);

            Serial.println("To SubState::PICK_GLASSES_PILE_5_FIND_NEXT_GLASSES_PILE_1_WAIT_FOR_T_AXIS");
            _subState = PICK_GLASSES_PILE_5_FIND_NEXT_GLASSES_PILE_1_WAIT_FOR_T_AXIS;
        }

        break;

        case SubState::PICK_GLASSES_PILE_5_FIND_NEXT_GLASSES_PILE_1_WAIT_FOR_T_AXIS:

            if (_tAxis.state() == LaddLib::Component::RotatingAxisComponent::State::IS_BETWEEN || _tAxis.state() == LaddLib::Component::RotatingAxisComponent::State::IS_ON_REF)
            {
                _currentGlassesPilesIndex = _currentGlassesPilesIndex == 0 ? NUM_GLASSES_PILES - 1 : _currentGlassesPilesIndex - 1;
                if (_glassesPiles[_currentGlassesPilesIndex])
                {
                    _zAxis.goUp();
                    Serial.println(_currentGlassesPilesIndex);
                    Serial.println("To SubState::PICK_GLASSES_PILE_6_ZAXIS_TO_MAX");
                    _subState = SubState::PICK_GLASSES_PILE_6_ZAXIS_TO_MAX;
                }
                else
                {
                    Serial.println(_currentGlassesPilesIndex);
                    Serial.println("To SubState::PICK_GLASSES_PILE_5_FIND_NEXT_GLASSES_PILE");
                    _subState = SubState::PICK_GLASSES_PILE_5_FIND_NEXT_GLASSES_PILE;
                }
            }

            break;

        case SubState::PICK_GLASSES_PILE_6_ZAXIS_TO_MAX:
            if (_zAxis.state() == LaddLib::Component::AxisComponent::IS_UP)
            {
                Serial.println("To SubState::PICK_GLASSES_PILE_7_CLOSE_GRIP1");
                _subState = SubState::PICK_GLASSES_PILE_7_CLOSE_GRIP1;
            }
            break;

        case SubState::PICK_GLASSES_PILE_7_CLOSE_GRIP1:
            _grip1Relay.activate();
            _yAxis.goDown();

            Serial.println("To SubState::PICK_GLASSES_PILE_8_YAXIS_TO_MIN");
            _subState = SubState::PICK_GLASSES_PILE_8_YAXIS_TO_MIN;

            break;

        case SubState::PICK_GLASSES_PILE_8_YAXIS_TO_MIN:
            if (_yAxis.state() == LaddLib::Component::AxisComponent::IS_DOWN)
            {
                _dAxis.goDown();
                Serial.println("To SubState::PICK_GLASSES_PILE_9_DAXIS_TO_MIN");
                _subState = SubState::PICK_GLASSES_PILE_9_DAXIS_TO_MIN;
            }

            break;

        case SubState::PICK_GLASSES_PILE_9_DAXIS_TO_MIN:
            if (_dAxis.state() == LaddLib::Component::AxisComponent::IS_DOWN)
            {
                Serial.println("To SubState::PICK_GLASSES_PILE_10_OPEN_GRIP1");
                _subState = SubState::PICK_GLASSES_PILE_10_OPEN_GRIP1;
            }
            break;

        case SubState::PICK_GLASSES_PILE_10_OPEN_GRIP1:
            _grip1Relay.deactivate();
            _zAxis.goDown();

            Serial.println("To SubState::PICK_GLASSES_PILE_11_ZAXIS_TO_MIN");
            _subState = SubState::PICK_GLASSES_PILE_11_ZAXIS_TO_MIN;
            break;

        case SubState::PICK_GLASSES_PILE_11_ZAXIS_TO_MIN:
            if (_zAxis.state() == LaddLib::Component::AxisComponent::IS_DOWN)
            {
                Serial.println("To State::IDLE");
                _subState = SubState::NONE_NONE;
                _state = State::IDLE;
            }
            break;
        default:
            break;
        }
    }
    else if (_state == State::COUNT_GLASSES_PILES)
    {
        switch (_subState)
        {
        case COUNT_GLASSES_PILES_0_INIT:
            _zAxis.goDown();
            Serial.println("To SubState::COUNT_GLASSES_PILES_1_Z_AXIS_TO_MIN");
            _subState = COUNT_GLASSES_PILES_1_Z_AXIS_TO_MIN;
            break;

        case COUNT_GLASSES_PILES_1_Z_AXIS_TO_MIN:
            if (_zAxis.state() == LaddLib::Component::AxisComponent::IS_DOWN)
            {
                _tAxis.rotateReverse();

                Serial.println("To SubState::COUNT_GLASSES_PILES_2_T_AXIS_TO_REF");
                _subState = COUNT_GLASSES_PILES_2_T_AXIS_TO_REF;
            }
            break;

        case COUNT_GLASSES_PILES_2_T_AXIS_TO_REF:
            if (_tAxis.state() == LaddLib::Component::RotatingAxisComponent::IS_ON_REF)
            {
                _currentGlassesPilesIndex = 0;
                _glassesPilesMeasured = false;

                Serial.println("To SubState::COUNT_GLASSES_PILES_3_ROTATE_AND_COUNT");
                _subState = SubState::COUNT_GLASSES_PILES_3_ROTATE_AND_COUNT;
            }
            break;

        case COUNT_GLASSES_PILES_3_ROTATE_AND_COUNT:
        {
            _tAxis.rotateNumRev(-1800);

            Serial.println("To SubState::COUNT_GLASSES_PILES_3_ROTATE_AND_COUNT_WAIT_FOR_T_AXIS");
            _subState = SubState::COUNT_GLASSES_PILES_4_ROTATE_AND_COUNT_WAIT_FOR_T_AXIS;
        }
        break;

        case COUNT_GLASSES_PILES_4_ROTATE_AND_COUNT_WAIT_FOR_T_AXIS:
            if (_tAxis.state() == LaddLib::Component::RotatingAxisComponent::IS_BETWEEN)
            {
                uint32_t distanceMm = _getUltrasonic1Measure();
                _glassesPiles[_currentGlassesPilesIndex] = distanceMm < Config::ultrasonic1DetectionDistance;

                _currentGlassesPilesIndex++;
                _subState = COUNT_GLASSES_PILES_3_ROTATE_AND_COUNT;

                if (_currentGlassesPilesIndex >= NUM_GLASSES_PILES)
                {
                    _currentGlassesPilesIndex = 0;
                    _glassesPilesMeasured = true;

                    Serial.println("To State::IDLE");
                    _subState = SubState::NONE_NONE;
                    _state = State::IDLE;
                }
                else
                {
                    Serial.println("To SubState::COUNT_GLASSES_PILES_3_ROTATE_AND_COUNT");
                    _subState = COUNT_GLASSES_PILES_3_ROTATE_AND_COUNT;
                }
            }
            break;

        default:
            break;
        }
    }
    else if (_state == State::ROTATE_GLASSES_PILE)
    {
        switch (_subState)
        {
        case SubState::ROTATE_GLASSES_PILE_0_INIT:
            Serial.println("To SubState::ROTATE_GLASSES_PILE_0_INIT");
            _subState = SubState::ROTATE_GLASSES_PILE_1_ROTATE;
            _tAxis.rotateNumRev(-1800);

            break;

        case SubState::ROTATE_GLASSES_PILE_1_ROTATE:
            if (_tAxis.state() == LaddLib::Component::RotatingAxisComponent::State::IS_BETWEEN || _tAxis.state() == LaddLib::Component::RotatingAxisComponent::State::IS_ON_REF)
            {
                Serial.println("To State::IDLE");
                _state = State::IDLE;
                _subState = SubState::NONE_NONE;
            }
            break;
        default:
            break;
        }
    }
    else if (_state == State::TEST)
    {
        switch (_subState)
        {
        case TEST_INIT_1:
            Serial.println("To SubState::TEST_INIT_2");
            _subState = SubState::TEST_INIT_2;

            _rAxis.reset();

            break;

        case TEST_INIT_2:
            if (_rAxis.isReady())
            {
                _rAxis.moveNumRev(Config::rQuarterNumRev);

                Serial.println("To SubState::TEST_INIT_3");
                _subState = SubState::TEST_INIT_3;
            }

            break;

        case TEST_INIT_3:
            if (_rAxis.state() == LaddLib::Component::AxisComponentTest::State::IS_BETWEEN)
            {   
                _rAxis.moveNumRev(-Config::rQuarterNumRev);

                Serial.println("To State::IDLE");
                _subState = SubState::NONE_NONE;
                _state = State::IDLE;
            }

            break;

        default:
            break;
        }
    }
}

void MasterController::onPause()
{
}

void MasterController::onResume()
{
}

uint32_t MasterController::_getUltrasonic1Measure()
{
    digitalWrite(Config::ultrasonic1TriggerPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(Config::ultrasonic1TriggerPin, LOW);

    /* 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe) */
    long measure = pulseIn(Config::ultrasonic1EchoPin, HIGH, Config::ultrasonic1Timeout);

    /* 3. Calcul la distance à partir du temps mesuré */
    uint32_t distance_mm = (int)(measure / 2.0 * 340.0 / 1000);

    if (distance_mm < 1)
    {
        distance_mm = 9999;
    }

    return distance_mm;
}

uint32_t MasterController::_getUltrasonic2Measure()
{
    digitalWrite(Config::ultrasonic2TriggerPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(Config::ultrasonic2TriggerPin, LOW);

    /* 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe) */
    long measure = pulseIn(Config::ultrasonic2EchoPin, HIGH, Config::ultrasonic2Timeout);

    /* 3. Calcul la distance à partir du temps mesuré */
    float distance_mm = (int)(measure / 2.0 * 340.0 / 1000);

    if (distance_mm < 1)
    {
        distance_mm = 9999;
    }

    return distance_mm;
}

uint32_t MasterController::_getUltrasonic3Measure()
{
    digitalWrite(Config::ultrasonic3TriggerPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(Config::ultrasonic3TriggerPin, LOW);

    /* 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe) */
    long measure = pulseIn(Config::ultrasonic3EchoPin, HIGH, Config::ultrasonic3Timeout);

    /* 3. Calcul la distance à partir du temps mesuré */
    float distance_mm = (int)(measure / 2.0 * 340.0 / 1000);

    if (distance_mm < 1)
    {
        distance_mm = 9999;
    }

    return distance_mm;
}

void MasterController::onInputNewLineCallback(LaddLib::SerialController::NewLineEvent &evt,
                                              LaddLib::Eventable *issuer, void *data)
{
    auto obj = (MasterController *)data;

    auto line = String("") + evt.getLine();
    if (line == "model")
    {
        Serial.println(String("model:ok:") + Config::MODEL);
    }
    else if (line == "status")
    {
        if (obj->_state == State::NONE)
        {
            Serial.println("status:ok:NONE");
        }
        else if (obj->_state == State::IDLE)
        {
            Serial.println("status:ok:IDLE");
        }
        else if (obj->_state == State::PICK_GLASS)
        {
            String str = "status:ok:PICK_GLASS:";
            
            switch(obj->_subState) 
            {   
                case SubState::PICK_GLASS_0_INIT:
                case SubState::PICK_GLASS_1_DISTRIBUTE_BEER:
                case SubState::PICK_GLASS_1_DAXIS_TO_GRIP_POS:
                case SubState::PICK_GLASS_2_CLOSE_GRIP1:
                case SubState::PICK_GLASS_3_DISTRIBUTE_BEER:
                    str += "DISTRIBUTE_BEER";
                    break;
                case SubState::PICK_GLASS_8_WAIT_FOR_GLASS_TO_BE_TAKEN:
                    str += "WAIT_FOR_GLASS_TO_BE_TAKEN";
                    break;
                default:
                    str += "OTHER";
                    break;
            }

            Serial.println(str); 
        }
        else if (obj->_state == State::PICK_GLASS_RETURN)
        {
            Serial.println("status:ok:PICK_GLASS_RETURN:");
        }
        else if (obj->_state == State::PICK_GLASSES_PILE)
        {
            Serial.println("status:ok:PICK_GLASSES_PILE:");
        }
        else if (obj->_state == State::ROTATE_GLASSES_PILE)
        {
            Serial.println("status:ok:ROTATE_GLASSES_PILE:");
        }
        else if (obj->_state == State::TEST)
        {
            Serial.println("status:ok:TEST:");
        }
        else if (obj->_state == State::INIT)
        {
            Serial.println("status:ok:INIT:");
        }
        else
        {
            Serial.println("status:ok:OTHER:");
        }
    }
    else if (line == "isReadyForReturn")
    {
        if (obj->_state == State::IDLE && !obj->_emptyEntrySwitch.isPushed())
        {
            Serial.println("isReadyForReturn:ok:1");
        }
        else
        {
            Serial.println("isReadyForReturn:ok:0");
        }
    }
    else if (line == "init")
    {
        if (obj->_state != State::NONE && obj->_state != State::IDLE)
        {
            Serial.println("init:ko:Not in IDLE state");
            return;
        }

        Serial.println("To State::INIT");
        obj->_state = State::INIT;
        Serial.println("To SubState::INIT_0_INIT");
        obj->_subState = SubState::INIT_0_INIT;

        Serial.println("init:ok:INIT");
        
    }
    else if (line.startsWith("pickglass:"))
    {
        unsigned char pos1 = (unsigned char)line.indexOf(':');
        unsigned char pos2 = (unsigned char)line.indexOf(':', pos1 + 1);
        unsigned char pos3 = (unsigned char)line.indexOf(':', pos2 + 1);

        unsigned int distributorPos = (unsigned char)line.substring(pos1 + 1, pos2).toInt();
        unsigned int numToServe = (unsigned char)line.substring(pos2 + 1, pos3).toInt();
        unsigned int qtyToDistribute = (unsigned char)line.substring(pos3 + 1).toInt();

        if (distributorPos > 0)
        {
            Serial.println("pickglass:ko:Invalid distributor position");
            return;
        }

        if (numToServe > 10)
        {
            Serial.println("pickglass:ko:Too many glasses to serve");
            return;
        }

        if (qtyToDistribute > 50)
        {
            Serial.println("pickglass:ko:Too much to distribute");
            return;
        }

        if (obj->_state == State::IDLE)
        {
            obj->_distributorPos = distributorPos;
            obj->_numToServe = numToServe;
            obj->_qtyToDistribute = qtyToDistribute;
            obj->_remainingToServe = numToServe;
        }
        else
        {
            Serial.println("pickglass:ko:State is not IDLE");
            return;
        }

#if LADDDEBUG_COMMANDS
        {
            char buffer[150];
            sprintf(buffer, "DEBUG:Call pickglass from input, distributorPos=%d, numToServe=%d, qtyToDistribute=%d", distributorPos, numToServe, qtyToDistribute);
            Serial.println(buffer);
        }
#endif

        Serial.println("pickglass:ok");
    }
    else if (line.startsWith("pickglassreturn:"))
    {
        unsigned char pos1 = (unsigned char)line.indexOf(':');
        unsigned char pos2 = (unsigned char)line.indexOf(':', pos1 + 1);
        unsigned char pos3 = (unsigned char)line.indexOf(':', pos2 + 1);

        unsigned int distributorPos = (unsigned char)line.substring(pos1 + 1, pos2).toInt();
        unsigned int numToServe = (unsigned char)line.substring(pos2 + 1, pos3).toInt();
        unsigned int qtyToDistribute = (unsigned char)line.substring(pos3 + 1).toInt();

        if(numToServe != 1) 
        {
            Serial.println("pickglassreturn:ko:Only one glass can be returned");
            return;
        }

        if (distributorPos > 0)
        {
            Serial.println("pickglassreturn:ko:Invalid distributor position");
            return;
        }

        if (qtyToDistribute > 50)
        {
            Serial.println("pickglassreturn:ko:Too much to distribute");
            return;
        }

        if (obj->_state == State::IDLE)
        {
            obj->_distributorPos = distributorPos;
            obj->_numToServe = numToServe;
            obj->_qtyToDistribute = qtyToDistribute;
            obj->_remainingToServe = numToServe;
        }
        else
        {
            Serial.println("pickglassreturn:ko:State is not IDLE");
            return;
        }

#if LADDDEBUG_COMMANDS
        {
            char buffer[150];
            sprintf(buffer, "DEBUG:Call pickglassreturn from input, distributorPos=%d, numToServe=%d, qtyToDistribute=%d", distributorPos, numToServe, qtyToDistribute);
            Serial.println(buffer);
        }
#endif

        obj->_state = State::PICK_GLASS_RETURN;
        obj->_subState = SubState::PICK_GLASS_RETURN_0_INIT;
        Serial.println("pickglassreturn:ok");
    }
    else
    {
        Serial.println(line + ':' + "ko:Unknown command");
    }
}

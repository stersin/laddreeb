#pragma once

namespace Config
{
    static const constexpr char *MODEL = "ladd_reeb";
    static const constexpr char *DEFAULT_ID = "0000";

    static const uint32_t SERIAL_BAUDRATE = 115200;

    static const uint8_t emergSwitchPin = 10;
    static const uint8_t processSwitchPin = 2;
    static const uint8_t process2SwitchPin = 7;

    static const uint8_t rotatePileSwitchPin = 47;

    // D axis
    static const uint8_t dDirPin = 22;
    static const uint8_t dStepPin = 23;
    static const uint8_t dBottomSwitchPin = 42;
    static const uint8_t dTopSwitchPin = 38;
    static const uint32_t dSpeed = 10;
    static const uint32_t dDown1DestRev = 300;

    static const float dDistributeBeerAxisSpeedRatio = 1.0;
    static const uint32_t dDistributeBeer1DelayMs = 8000000;
    static const uint32_t dDistributeBeer2DelayMs = 500000;

    //static const float dDistributeBeerAxisSpeedRatio = 0.025;
    //static const uint32_t dDistributeBeer1DelayMs = 4000000;
    //static const uint32_t dDistributeBeer2DelayMs = 4000000;

    static const uint8_t dDistributeBeerRelayPin = 5;
    static const bool dDistributeBeerRelayInverted = false;

    static const bool dReverseDir = false;

    // Rotation
    static const uint8_t rDirPin = 24;
    static const uint8_t rStepPin = 25;
    static const uint32_t rSpeed = 10;
    static const uint32_t rMaxNumRev = 0;
    static const uint32_t rQuarterNumRev = 3200;
    static const bool rReverseDir = false;

    // z axis
    static const uint8_t zDirPin = 26;
    static const uint8_t zStepPin = 27;
    static const uint8_t zBottomSwitchPin = 40;
    static const uint8_t zTopSwitchPin = 41;
    static const uint32_t zSpeed = 10;
    static const bool zReverseDir = true;

    // y axis
    static const uint8_t yDirPin = 28;
    static const uint8_t yStepPin = 29;
    static const uint8_t yBottomSwitchPin = 43;
    static const uint8_t yTopSwitchPin = 39;
    static const uint32_t ySpeed = 10;
    static const bool yReverseDir = false;

    // t axis
    static const uint8_t tDirPin = 32;
    static const uint8_t tStepPin = 33;
    static const uint32_t tSpeed = 150;
    static const uint32_t tMaxNumRev = 0;
    static const uint8_t tRevSensor = 3;
    static const bool tReverseDir = true;

    // grip 1
    static const uint8_t grip1RelayPin = 4;
    static const bool grip1RelayInverted = false;
    
    // empty pile
    static const uint8_t emptyPileSwitchPin = 44;

    // empty entry
    static const uint8_t emptyEntrySwitchPin = 16;

    // ultrasonic1
    static const unsigned long ultrasonic1Timeout = 25000UL;
    static const uint8_t ultrasonic1TriggerPin = 12;
    static const uint8_t ultrasonic1EchoPin = 13;
    static const uint8_t ultrasonic1DetectionDistance = 60;

    // ultrasonic2
    static const unsigned long ultrasonic2Timeout = 25000UL;
    static const uint8_t ultrasonic2TriggerPin = 19;
    static const uint8_t ultrasonic2EchoPin = 20;
    static const uint8_t ultrasonic2DetectionDistance = 100;

    // ultrasonic2
    static const unsigned long ultrasonic3Timeout = 25000UL;
    static const uint8_t ultrasonic3TriggerPin = 17;
    static const uint8_t ultrasonic3EchoPin = 18;
    static const uint32_t ultrasonic3WorkingRangeMin = 200;
    static const uint32_t ultrasonic3WorkingRangeMax = 400;
    static const uint32_t ultrasonic3DetectionDistance = 280;
};

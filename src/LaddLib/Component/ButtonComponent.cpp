#include "ButtonComponent.h"

#define LADDDEBUG_PREFIX "DEBUG:BtnComp"
#define LADDDEBUG 1

using LaddLib::Component::ButtonComponent;

ButtonComponent::ButtonComponent(
    const char *id,
    uint8_t sensorPin,
    bool useInternalPullup,
    bool invertedMode)
    : 
    Identifiable(id),
    DigitalInputComponent(
          sensorPin,
          useInternalPullup,
          invertedMode)
{
}

void ButtonComponent::onInit()
{
    #if LADDDEBUG
        Serial.println(LADDDEBUG_PREFIX"::onInit");
    #endif

    DigitalInputComponent::onInit();

    if(!isUsable()) {
    #if LADDDEBUG
        Serial.println(LADDDEBUG_PREFIX"::isUsable() == false");
    #endif
    }

    this->_buttonState = this->_lastButtonState = false;
}

void ButtonComponent::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
    this->_stateChanged = false;

    _hasInput= this->hasInput();

    if (_hasInput != this->_lastButtonState)
    {
        // reset the debouncing timer
        this->_lastDebounceTime = millis();
    }

    if ((millis() - this->_lastDebounceTime) > this->_debounceDelay)
    {
        // if the button state has changed:
        if (_hasInput != this->_buttonState)
        {
            this->_buttonState = _hasInput;
            this->_stateChanged = true;

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s Button state changed, state=%s", getId(), this->_buttonState ? "on" : "off");
                Serial.println(buffer);
            }
#endif
        }
    }

    // save the reading. Next time through the loop, it'll be the
    // lastButtonState:
    this->_lastButtonState = _hasInput;
}

bool ButtonComponent::stateChanged() { return this->_stateChanged; }

bool ButtonComponent::stateChangedToPushed()
{
    return this->stateChanged() && _hasInput;
}

bool ButtonComponent::stateChangedToReleased()
{
    return this->stateChanged() && !_hasInput;
}

bool ButtonComponent::isPushed()
{
    return _hasInput;
}
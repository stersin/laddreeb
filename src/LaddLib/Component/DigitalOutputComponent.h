#pragma once

#include <Arduino.h>
#include "../Initializable.h"

namespace LaddLib {
namespace Component {
class DigitalOutputComponent: public Initializable {
public:
    enum State {
        UNKNOWN,
        ACTIVATED,
        DEACTIVATED
    };

    DigitalOutputComponent(uint8_t controlPin, bool invertedMode = false);

    bool isUsable();
    void activate();
    void deactivate();

    State getState();

protected:
    virtual void onInit() override;

private:
    uint8_t _controlPin;
    bool _invertedMode;
    State _state;
};
}
}

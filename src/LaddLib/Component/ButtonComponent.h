#pragma once

#include <Arduino.h>
#include "./DigitalInputComponent.h"
#include "../Updatable.h"
#include "../Identifiable.h"

namespace LaddLib {
namespace Component {
class ButtonComponent: public Identifiable<16>, public DigitalInputComponent, public Updatable {
public:
    ButtonComponent(const char* id, uint8_t sensorPin, bool useInternalPullup = true, bool invertedMode = false);

    bool isPushed();

    bool stateChanged();

    bool stateChangedToPushed();

    bool stateChangedToReleased();

protected:
    virtual void onInit() override;
    virtual void onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis) override;

private:
  bool _stateChanged = false;
  bool _hasInput = false;
  int _buttonState = HIGH;              // the current reading from the input pin
  int _lastButtonState = HIGH;          // the previous reading from the input pin

  unsigned long _lastDebounceTime = 0;  // the last time the output pin was toggled
  unsigned long _debounceDelay = 50;    // the debounce time; increase if the output flickers


};
}
}

#pragma once

#if ENABLE_DHT11

#include <Arduino.h>
#include <dht11.h>
#include "../Initializable.h"

namespace LaddLib {
namespace Component {
class DHT11Component: public Initializable {
public:
    DHT11Component(uint8_t sensorPin);

    void updateData();

    float getTemperature();

    float getHumidity();

    int getStatus();

    bool isStatusOk();

    void debug();

protected:
    virtual void onInit() override;

private:
    uint8_t _sensorPin;
    float _temperature;
    float _humidity;
    int _status;
    int _lastUpdateTimems = 0;
    dht11 _dht11;
};
}
}

#endif
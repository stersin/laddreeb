#pragma once

#include <Arduino.h>
#include "../Initializable.h"
#include "../Updatable.h"
#include "DigitalOutputComponent.h"

namespace LaddLib {
namespace Component {
class DigitalOutputDelayComponent: public DigitalOutputComponent, public Updatable {
public:
    DigitalOutputDelayComponent(uint8_t controlPin, bool invertedMode = false);

    void activateWithDelay(uint32_t delayMs);

protected:
    virtual void onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis) override;
private:
    uint32_t _delayMs;
    uint32_t _lastActivationTime;
};
}
}

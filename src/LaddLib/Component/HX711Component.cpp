#if ENABLE_HX711

#include "HX711Component.h"

using LaddLib::Component::HX711Component;

HX711Component::HX711Component(uint8_t dataPin, uint8_t clockPin, float scale, uint8_t gain)
        :
        _dataPin(dataPin), _clockPin(clockPin), _scale(scale), _gain(gain)
{
}

void HX711Component::updateData(byte times, bool byPassRefreshDelay)
{
    if (!isUsable())
        return;

    int timems = millis();

    // Do not update too often
    if (byPassRefreshDelay || _lastUpdateTimems==0 || timems-_lastUpdateTimems>100)
    {
        _hx711.power_up();
        _weight = _hx711.get_units(times);
        _hx711.power_down();
    }

    _lastUpdateTimems = millis();
}

void HX711Component::calibrate()
{
    if (!isUsable())
        return;

    _hx711.set_scale();
    tare();

    Serial.println("You Have 5s to place a known weight on the scale :");
    delay(5000);

    Serial.println("10 averaged readings will be output :");

    for (int i = 0; i<10; i++)
    {
        _hx711.power_up();
        Serial.println(_hx711.get_units(10));
        _hx711.power_down();

        delay(1000);
    }
}

void HX711Component::tare()
{
    _hx711.power_up();
    _hx711.tare(10);
    _hx711.power_down();
}

float HX711Component::getWeight() const
{
    return _weight;
}

bool HX711Component::isUsable() const
{
    return _dataPin>0 && _clockPin>0;
}

void HX711Component::onInit()
{
    if (!isUsable())
        return;

#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::onInit started");
#endif

    _hx711.begin(_dataPin, _clockPin, _gain);
    _hx711.set_scale(_scale);
    tare();

#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::onInit ended");
#endif
}

#endif
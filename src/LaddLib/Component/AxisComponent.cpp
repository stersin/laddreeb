#include "AxisComponent.h"
#include "ButtonComponent.h"
#include <math.h>

#define LADDDEBUG_PREFIX "DEBUG:AxisComp"
#define LADDDEBUG 1

using LaddLib::Component::AxisComponent;

AxisComponent::AxisComponent(
    const char *id,
    uint8_t dirPin,
    uint8_t stepPin,
    uint32_t speedDelay,
    bool reverseDir,
    ButtonComponent *bottomSwitch,
    ButtonComponent *topSwitch,
    uint32_t maxNumRev)
    : Identifiable(id),
      _dirPin(dirPin),
      _stepPin(stepPin),
      _speedDelay(speedDelay),
      _maxNumRev(maxNumRev),
      _reverseDir(reverseDir),
      _bottomSwitch(bottomSwitch),
      _topSwitch(topSwitch)
{
    this->_maxNumRev = 1000000000;
}

void AxisComponent::onInit()
{
    if (isUsable())
    {
        pinMode(this->_dirPin, OUTPUT);
        pinMode(this->_stepPin, OUTPUT);
        // Set the spinning direction CW/CCW:
        digitalWrite(this->_dirPin, _reverseDir ? HIGH : LOW);
    }
}

void AxisComponent::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
    if (this->_state == State::INIT)
    {
        if (_bottomSwitch)
        {
            this->_state = State::GO_DOWN;

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::GO_DOWN (until bottom switch reached)", getId());
                Serial.println(buffer);
            }
#endif
        }
        else
        {
            this->_state = State::IS_DOWN;
            this->_numRev = 0;
            this->_isReady = true;

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_DOWN (no ref)", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
    else if (this->_state == State::GO_DOWN)
    {
        if (this->_bottomSwitch && this->_bottomSwitch->isPushed())
        {
            this->_state = State::IS_DOWN;
            this->_numRev = 0;
            this->_isReady = true;

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_DOWN (bottom switch reached)", getId());
                Serial.println(buffer);
            }
#endif
        }
        else if (!this->_isReady || this->_destRev < this->_numRev || this->_destRev == 0)
        {
            this->move(Direction::DOWN, this->_speedRatio);

            if (this->_isReady && this->_numRev > 0)
            {
                this->_numRev--;
            }
        }
        else if(_isReady && this->_destRev == this->_numRev)
        {

            this->_state = State::IS_BETWEEN;
#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_BETWEEN (go down destRev reached)", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
    else if (this->_state == State::GO_UP)
    {
        if (this->_topSwitch && this->_topSwitch->isPushed())
        {
            this->_state = State::IS_UP;

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_UP (this->_topSwitch && this->_topSwitch->isPushed())", getId());
                Serial.println(buffer);
            }
#endif
        }
        else if (this->_maxNumRev > 0 && this->_numRev >= this->_maxNumRev)
        {
            this->_state = State::IS_UP;
            this->_numRev = this->_maxNumRev;

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_UP (Status changed to State::IS_UP (this->_numRev >= this->_maxNumRev))", getId());
                Serial.println(buffer);
            }
#endif
        }
        else if (this->_destRev > this->_numRev)
        {
            this->move(Direction::UP, this->_speedRatio);
            this->_numRev++;
        }
        else
        {
            this->_state = State::IS_BETWEEN;
#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_BETWEEN (1)", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
    else if (this->_state == State::IS_BETWEEN)
    {
        if (this->_destRev == 0)
        {
            this->_state = State::GO_DOWN;
#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::GO_DOWN", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
}

bool AxisComponent::isUsable() const
{
    return _dirPin > 0 && _stepPin > 0;
}

bool AxisComponent::isReady() const
{
    return _isReady;
}

void AxisComponent::move(AxisComponent::Direction dir, float speedRatio)
{
    if (dir != this->_dir)
    {
#if LADDDEBUG
        {
            char buffer[256];
            sprintf(buffer, LADDDEBUG_PREFIX ":%s::move => Direction changed", getId());
            Serial.println(buffer);
        }
#endif
        this->_dir = dir;

        // Set the spinning direction CW/CCW:
        digitalWrite(this->_dirPin, dir == Direction::UP ? (this->_reverseDir ? LOW : HIGH) : (this->_reverseDir ? HIGH : LOW));
    }

    digitalWrite(this->_stepPin, HIGH);
    delayMicroseconds((int) round(this->_speedDelay / speedRatio));
    digitalWrite(this->_stepPin, LOW);
    delayMicroseconds((int) round(this->_speedDelay / speedRatio));
}

void AxisComponent::goUp(float speedRatio)
{
    this->moveToRev(this->_maxNumRev, speedRatio);
}

void AxisComponent::moveToRev(int32_t destRev, float speedRatio)
{
    this->_speedRatio = speedRatio;
    this->_destRev = destRev;

    if (this->_numRev != destRev)
    {
        if (this->_numRev < destRev)
        {
            this->_state = State::GO_UP;
        }
        else
        {
            this->_state = State::GO_DOWN;
        }
    }
}

void AxisComponent::moveNumRev(int32_t numRev, float speedRatio)
{
    this->_speedRatio = speedRatio;

    this->_destRev += numRev;

    if (this->_numRev != _destRev)
    {
        if (this->_numRev < _destRev)
        {
            this->_state = State::GO_UP;
        }
        else
        {
            this->_state = State::GO_DOWN;
        }
    }
}

void AxisComponent::goDown(float speedRatio)
{
    this->moveToRev(0, speedRatio);
}
void AxisComponent::emerg() { this->_state = State::ERROR; }

void AxisComponent::reset()
{
    this->_speedRatio = 1.0;
    this->_state = State::INIT;
    this->_numRev = this->_destRev = 0;
    this->_dir = Direction::DOWN;
    digitalWrite(this->_dirPin, _reverseDir ? HIGH : LOW);
}

AxisComponent::State AxisComponent::state()
{
    return this->_state;
}

AxisComponent::Direction AxisComponent::direction()
{
    return this->_dir;
}

bool AxisComponent::isDestRevReached(int32_t destRev)
{
    return this->_numRev == this->_destRev && this->_destRev == destRev;
}

void AxisComponent::setSpeedRatio(float speedRatio)
{
    this->_speedRatio = speedRatio;
}
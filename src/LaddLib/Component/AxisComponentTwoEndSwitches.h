#pragma once

#include <Arduino.h>
#include "./ButtonComponent.h"
#include "../Updatable.h"
#include "../Identifiable.h"
#include "./AxisComponent.h"

namespace LaddLib
{
  namespace Component
  {
    class AxisComponentTwoEndSwitches : public AxisComponent
    {
    public:
      AxisComponentTwoEndSwitches(const char *id, uint8_t dirPin, uint8_t stepPin, uint32_t speedDelay, bool reverseDir = false, ButtonComponent *bottomSwitch = NULL, ButtonComponent *topSwitch = NULL);
      

    protected:
      virtual void onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis) override;
    };
  }
}

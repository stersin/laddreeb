#pragma once

#if ENABLE_HX711
#include <Arduino.h>
#include "../Initializable.h"

#include "HX711.h"

namespace LaddLib {
namespace Component {
class HX711Component: public Initializable {
public:
    HX711Component(uint8_t dataPin, uint8_t clockPin, float scale, uint8_t gain = 128);

    void updateData(byte times = 5, bool byPassRefreshDelay = false);
    void calibrate();
    void tare();
    float getWeight() const;
    bool isUsable() const;

protected:
    virtual void onInit() override;

private:
    uint8_t _dataPin;
    uint8_t _clockPin;
    float _scale;
    uint8_t _gain;

    HX711 _hx711;

    int _lastUpdateTimems = 0;
    float _weight;
};
}
}
#endif
#include "AxisComponentTwoEndSwitches.h"
#include "ButtonComponent.h"

#define LADDDEBUG_PREFIX "DEBUG:AxisComp"
#define LADDDEBUG 1

using LaddLib::Component::AxisComponentTwoEndSwitches;

AxisComponentTwoEndSwitches::AxisComponentTwoEndSwitches(
    const char *id,
    uint8_t dirPin,
    uint8_t stepPin,
    uint32_t speedDelay,
    bool reverseDir,
    ButtonComponent *bottomSwitch,
    ButtonComponent *topSwitch)
    : AxisComponent(id, dirPin, stepPin, speedDelay, reverseDir, bottomSwitch, topSwitch)
{
}

void AxisComponentTwoEndSwitches::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
    if (this->_state == State::INIT)
    {
        if (_bottomSwitch)
        {
            this->_state = State::GO_DOWN;

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::GO_DOWN (until bottom switch reached)", getId());
                Serial.println(buffer);
            }
#endif
        }
        else
        {
            this->_state = State::IS_DOWN;
            this->_numRev = 0;
            this->_isReady = true;

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_DOWN (no ref)", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
    else if (this->_state == State::GO_DOWN)
    {
        if (this->_bottomSwitch && this->_bottomSwitch->isPushed())
        {
            this->_state = State::IS_DOWN;
            this->_numRev = this->_destRev = 0;
            this->_isReady = true;

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_DOWN (bottom switch reached)", getId());
                Serial.println(buffer);
            }
#endif
        }
        else if (!this->_isReady || this->_destRev < this->_numRev || this->_destRev == 0)
        {
            this->move(Direction::DOWN, this->_speedRatio);

            if (this->_isReady && this->_numRev > 0)
            {
                this->_numRev--;
            }
        }
        else if(_isReady && this->_destRev == this->_numRev)
        {

            this->_state = State::IS_BETWEEN;
#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_BETWEEN (go down destRev reached)", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
    else if (this->_state == State::GO_UP)
    {
        if (this->_topSwitch && this->_topSwitch->isPushed())
        {
            this->_state = State::IS_UP;
            this->_destRev = this->_numRev;

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_UP (this->_topSwitch && this->_topSwitch->isPushed())", getId());
                Serial.println(buffer);
            }
#endif
        }        
        else if (this->_destRev > this->_numRev)
        {
            this->move(Direction::UP, this->_speedRatio);
            this->_numRev++;
        }
        else
        {
            this->_state = State::IS_BETWEEN;
#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_BETWEEN (1)", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
    else if (this->_state == State::IS_BETWEEN)
    {
        if (this->_destRev == 0)
        {
            this->_state = State::GO_DOWN;
#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::GO_DOWN", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
}
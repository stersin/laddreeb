#pragma once

#include <Arduino.h>
#include "./ButtonComponent.h"
#include "../Updatable.h"
#include "../Identifiable.h"

namespace LaddLib
{
  namespace Component
  {
    class AxisComponentTest : public Identifiable<16>, public Initializable, public Updatable
    {
    public:
      enum State
      {
        UNKNOWN,
        INIT,
        GO_DOWN,
        IS_DOWN,
        GO_UP,
        IS_UP,
        IS_BETWEEN,
        ERROR
      };

      enum SubState
      {
        NONE,
        MOVING1,
        MOVING2
      };

      enum Direction
      {
        DOWN,
        UP
      };

      AxisComponentTest(const char *id, uint8_t dirPin, uint8_t stepPin, uint32_t speedDelay, bool reverseDir = false);
      bool isUsable() const;
      bool isReady() const;
      void move(AxisComponentTest::Direction dir, float speedRatio = 1.0);
      void goUp(float speedRatio = 1.0);
      void goDown(float speedRatio = 1.0);
      void reset();
      void moveToRev(int32_t destRev, float speedRatio = 1.0);
      void moveNumRev(int32_t numRev, float speedRatio = 1.0);
      bool isDestRevReached(int32_t destRev);
      Direction direction();
      State state();
      void emerg();

      void setSpeedRatio(float speedRatio);

    protected:
      virtual void onInit() override;
      virtual void onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis) override;

      uint8_t _dirPin;
      uint8_t _stepPin;
      uint32_t _speedDelay;
      bool _reverseDir = false;
      
      bool _isReady = false;
      int32_t _numRev = 0;
      int32_t _destRev = 0;
      float _speedRatio = 1.0;
      Direction _dir = Direction::DOWN;
      State _state = State::UNKNOWN;
      SubState _subState = SubState::NONE;
    };
  }
}

#include "AxisComponentTest.h"
#include "ButtonComponent.h"
#include <math.h>

#define LADDDEBUG_PREFIX "DEBUG:AxisComp"
#define LADDDEBUG 1

using LaddLib::Component::AxisComponentTest;

AxisComponentTest::AxisComponentTest(
    const char *id,
    uint8_t dirPin,
    uint8_t stepPin,
    uint32_t speedDelay,
    bool reverseDir)
    : Identifiable(id),
      _dirPin(dirPin),
      _stepPin(stepPin),
      _speedDelay(speedDelay),
      _reverseDir(reverseDir)
{
}

void AxisComponentTest::onInit()
{
    if (isUsable())
    {
        pinMode(this->_dirPin, OUTPUT);
        pinMode(this->_stepPin, OUTPUT);
        // Set the spinning direction CW/CCW:
        digitalWrite(this->_dirPin, _reverseDir ? HIGH : LOW);
    }

    this->_numRev = 0;
    this->_isReady = true;
    this->_state = State::IS_BETWEEN;
}

void AxisComponentTest::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
    if (this->_state == State::INIT)
    {
            this->_numRev = 0;
            this->_isReady = true;
            this->_state = State::IS_BETWEEN;

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_DOWN (no ref)", getId());
                Serial.println(buffer);
            }
#endif
    }
    else if (this->_state == State::GO_DOWN)
    {
        if (!this->_isReady || this->_destRev < this->_numRev)
        {
            this->move(Direction::DOWN, this->_speedRatio);

            if (this->_isReady)
            {
                this->_numRev--;
            }
        }
        else if(_isReady && this->_destRev == this->_numRev)
        {

            this->_state = State::IS_BETWEEN;
#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_BETWEEN (go down destRev reached)", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
    else if (this->_state == State::GO_UP)
    {
        if (this->_destRev > this->_numRev)
        {
            this->move(Direction::UP, this->_speedRatio);
            this->_numRev++;
        }
        else
        {
            this->_state = State::IS_BETWEEN;
#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_BETWEEN (1)", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
}

bool AxisComponentTest::isUsable() const
{
    return _dirPin > 0 && _stepPin > 0;
}

bool AxisComponentTest::isReady() const
{
    return _isReady;
}

void AxisComponentTest::move(AxisComponentTest::Direction dir, float speedRatio)
{
    if (dir != this->_dir)
    {
#if LADDDEBUG
        {
            char buffer[256];
            sprintf(buffer, LADDDEBUG_PREFIX ":%s::move => Direction changed", getId());
            Serial.println(buffer);
        }
#endif
        this->_dir = dir;

        // Set the spinning direction CW/CCW:
        digitalWrite(this->_dirPin, dir == Direction::UP ? (this->_reverseDir ? LOW : HIGH) : (this->_reverseDir ? HIGH : LOW));
    }

    digitalWrite(this->_stepPin, HIGH);
    delayMicroseconds((int) round(this->_speedDelay / speedRatio));
    digitalWrite(this->_stepPin, LOW);
    delayMicroseconds((int) round(this->_speedDelay / speedRatio));
}

void AxisComponentTest::moveToRev(int32_t destRev, float speedRatio)
{
    this->_speedRatio = speedRatio;
    this->_destRev = destRev;

    if (this->_numRev != destRev)
    {
        if (this->_numRev < destRev)
        {
            this->_state = State::GO_UP;
        }
        else
        {
            this->_state = State::GO_DOWN;
        }
    }
}

void AxisComponentTest::moveNumRev(int32_t numRev, float speedRatio)
{
    this->_speedRatio = speedRatio;

    this->_destRev += numRev;

    if (this->_numRev != _destRev)
    {
        if (this->_numRev < _destRev)
        {
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::moveNumRev => Status changed to State::GO_UP, %ld, %ld, %ld", getId(), numRev, _destRev, _numRev);
                Serial.println(buffer);
            }

            this->_state = State::GO_UP;
        }
        else
        {
            
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::moveNumRev => Status changed to State::GO_DOWN, %ld, %ld, %ld", getId(), numRev, _destRev, _numRev);
                Serial.println(buffer);
            }

            this->_state = State::GO_DOWN;
        }
    }
}

void AxisComponentTest::emerg() { this->_state = State::ERROR; }

void AxisComponentTest::reset()
{
    this->_speedRatio = 1.0;
    this->_state = State::IS_BETWEEN;
    this->_isReady = true;
    this->_numRev = this->_destRev = 0;
    this->_dir = Direction::DOWN;
    digitalWrite(this->_dirPin, _reverseDir ? HIGH : LOW);
}

AxisComponentTest::State AxisComponentTest::state()
{
    return this->_state;
}

AxisComponentTest::Direction AxisComponentTest::direction()
{
    return this->_dir;
}

bool AxisComponentTest::isDestRevReached(int32_t destRev)
{
    return this->_numRev == this->_destRev && this->_destRev == destRev;
}

void AxisComponentTest::setSpeedRatio(float speedRatio)
{
    this->_speedRatio = speedRatio;
}
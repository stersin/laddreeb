#include "DigitalInputComponent.h"

#define LADDDEBUG_PREFIX "DEBUG:DIComp"
#define LADDDEBUG 1

using LaddLib::Component::DigitalInputComponent;

DigitalInputComponent::DigitalInputComponent(uint8_t sensorPin, bool useInternalPullup, bool invertedMode)
        :
        _sensorPin(sensorPin), _useInternalPullup(useInternalPullup), _invertedMode(invertedMode)
{

}

bool DigitalInputComponent::isUsable() const
{
    return _sensorPin > 0;
}

bool DigitalInputComponent::hasInput() const
{
    if (!isInit())
    {
        Serial.println("ERR_ES_C_DIC_01");
        return false;
    }

    return isUsable() ? (bool)(digitalRead(_sensorPin)==
            _invertedMode ?
                           (_useInternalPullup ? HIGH : LOW) :
                           (_useInternalPullup ? LOW : HIGH)
    )
                        : false;
}

void DigitalInputComponent::onInit()
{
    #if LADDDEBUG
        Serial.println(LADDDEBUG_PREFIX"::onInit");
    #endif

    if (isUsable())
    {
        pinMode(_sensorPin, _useInternalPullup ? INPUT_PULLUP : INPUT);
    }
}

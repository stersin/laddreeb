#include "DigitalOutputDelayComponent.h"

using LaddLib::Component::DigitalOutputDelayComponent;

DigitalOutputDelayComponent::DigitalOutputDelayComponent(uint8_t controlPin, bool invertedMode)
        : DigitalOutputComponent(controlPin, invertedMode)
{
}

void DigitalOutputDelayComponent::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
    if(this->_lastActivationTime == 0 && this->_delayMs > 0) {
        this->_lastActivationTime = timeMillis;
        this->activate();
    }

    if (this->_lastActivationTime > 0 && timeMillis - this->_lastActivationTime >= this->_delayMs)
    {
        this->_lastActivationTime = 0;
        this->_delayMs = 0;
        this->deactivate();
    }
}

void DigitalOutputDelayComponent::activateWithDelay(uint32_t delayMs)
{
    this->_delayMs = delayMs;
    this->_lastActivationTime = 0;
}

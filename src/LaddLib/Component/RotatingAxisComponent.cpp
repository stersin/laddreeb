#include "RotatingAxisComponent.h"
#include "ButtonComponent.h"

#define LADDDEBUG_PREFIX "DEBUG:AxisComp"
#define LADDDEBUG 1

using LaddLib::Component::RotatingAxisComponent;

RotatingAxisComponent::RotatingAxisComponent(
    const char *id,
    uint8_t dirPin,
    uint8_t stepPin,
    uint32_t speedDelay,
    bool reverseDir,
    ButtonComponent *revSwitch)
    : Identifiable(id),
      _dirPin(dirPin),
      _stepPin(stepPin),
      _speedDelay(speedDelay),
      _reverseDir(reverseDir),
      _revSwitch(revSwitch)
{
}

void RotatingAxisComponent::onInit()
{
    if (isUsable())
    {
        pinMode(this->_dirPin, OUTPUT);
        pinMode(this->_stepPin, OUTPUT);
        // Set the spinning direction CW/CCW:
        digitalWrite(this->_dirPin, _reverseDir ? HIGH : LOW);
    }
}

void RotatingAxisComponent::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
    if(_leftSensor == false && this->_revSwitch && !this->_revSwitch->isPushed()) {
        _leftSensor = true;
    }

    if (this->_state == State::INIT)
    {
        if(this->_revSwitch && this->_revSwitch->isPushed()) 
        {
#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Must move outside from rev switch", getId());
                Serial.println(buffer);
            }
#endif
            while(this->_revSwitch->isPushed()) {
                this->move(Direction::FORWARD);
                this->_revSwitch->update();
            }
            
            for(int i = 0; i < 1500; i++) {
                this->move(Direction::FORWARD);
            }

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Moved outside from rev switch", getId());
                Serial.println(buffer);
            }
#endif
        }

        this->_state = State::ROTATE_REVERSED;

#if LADDDEBUG
        {
            char buffer[256];
            sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::ROTATE_REVERSED (until bottom switch reached)", getId());
            Serial.println(buffer);
        }
#endif        
    }
    else if (this->_state == State::ROTATE_REVERSED)
    {
        if (this->_revSwitch && this->_revSwitch->isPushed() && (!_isReady || this->_destRev == this->_numRev))
        {
            this->_state = State::IS_ON_REF;
            this->_numRev = _destRev = REV_REF;
            this->_isReady = true;

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_ON_REF (rev switch reached)", getId());
                Serial.println(buffer);
            }
#endif
        }
        else if (!this->_isReady || this->_destRev < this->_numRev || this->_destRev == REV_REF)
        {
            this->move(Direction::BACKWARD);

            if (this->_isReady)
            {
                this->_numRev--;
            }
        }
        else if(_isReady && this->_destRev == this->_numRev)
        {

            this->_state = State::IS_BETWEEN;
#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_BETWEEN (destRev reached)", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
    else if (this->_state == State::ROTATE)
    {
        if (this->_revSwitch && this->_revSwitch->isPushed())
        {
            this->_state = State::IS_ON_REF;

#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_ON_REF (this->_revSwitch && this->_revSwitch->isPushed())", getId());
                Serial.println(buffer);
            }
#endif
        }
        else if (this->_destRev > this->_numRev)
        {
            this->move(Direction::FORWARD);
            this->_numRev++;
        }
        else
        {
            this->_state = State::IS_BETWEEN;
#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::IS_BETWEEN (1)", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
    else if (this->_state == State::IS_BETWEEN)
    {
        if (this->_destRev == REV_REF)
        {
            this->_state = State::ROTATE_REVERSED;
#if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::ROTATE_REVERSED", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
}

bool RotatingAxisComponent::isUsable() const
{
    return _dirPin > 0 && _stepPin > 0;
}

bool RotatingAxisComponent::isReady() const
{
    return _isReady;
}

void RotatingAxisComponent::move(RotatingAxisComponent::Direction dir)
{
    if (dir != this->_dir)
    {
#if LADDDEBUG
        {
            char buffer[256];
            sprintf(buffer, LADDDEBUG_PREFIX ":%s::move => Direction changed", getId());
            Serial.println(buffer);
        }
#endif
        this->_dir = dir;

        // Set the spinning direction CW/CCW:
        digitalWrite(this->_dirPin, dir == Direction::FORWARD ? (this->_reverseDir ? LOW : HIGH) : (this->_reverseDir ? HIGH : LOW));
    }

    digitalWrite(this->_stepPin, HIGH);
    delayMicroseconds(this->_speedDelay);
    digitalWrite(this->_stepPin, LOW);
    delayMicroseconds(this->_speedDelay);
}

void RotatingAxisComponent::rotate()
{
    this->_state = State::ROTATE;
}

void RotatingAxisComponent::rotateReverse()
{
    this->_state = State::ROTATE_REVERSED;
}


void RotatingAxisComponent::rotateNumRev(int32_t numRev)
{
    {
        char buffer[256];
        sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::ROTATE, %ld, %ld, %ld", getId(), numRev, _destRev, _numRev);
        Serial.println(buffer);
    }
    _destRev += numRev;

    {
        char buffer[256];
        sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::ROTATE, %ld, %ld, %ld", getId(), numRev, _destRev, _numRev);
        Serial.println(buffer);
    }

    if (_destRev != _numRev)
    {
        if (_destRev > _numRev)
        {

            this->_state = State::ROTATE;

            #if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::ROTATE", getId());
                Serial.println(buffer);
            }
#endif
        }
        else
        {
            this->_state = State::ROTATE_REVERSED;

            #if LADDDEBUG
            {
                char buffer[256];
                sprintf(buffer, LADDDEBUG_PREFIX ":%s::onUpdate => Status changed to State::ROTATE_REVERSED", getId());
                Serial.println(buffer);
            }
#endif
        }
    }
}

void RotatingAxisComponent::emerg() { this->_state = State::ERROR; }

void RotatingAxisComponent::reset()
{
    this->_state = State::INIT;
    this->_numRev = this->_destRev = 0;
    this->_dir = Direction::BACKWARD;
    digitalWrite(this->_dirPin, _reverseDir ? HIGH : LOW);
}

RotatingAxisComponent::State RotatingAxisComponent::state()
{
    return this->_state;
}

RotatingAxisComponent::Direction RotatingAxisComponent::direction()
{
    return this->_dir;
}

bool RotatingAxisComponent::isDestRevReached(int32_t destRev)
{
    return this->_numRev == this->_destRev && this->_destRev == destRev;
}
#if ENABLE_DHT11

#include "DHT11Component.h"

using LaddLib::Component::DHT11Component;

DHT11Component::DHT11Component(uint8_t sensorPin)
        :_sensorPin(sensorPin)
{

}

void DHT11Component::updateData()
{
    int timems = millis();

    // Do not update too often
    if (_lastUpdateTimems==0 || timems-_lastUpdateTimems>500)
    {
        _status = _dht11.read(_sensorPin);

        if (_status==DHTLIB_OK)
        {
            _temperature = _dht11.temperature;
            _humidity = _dht11.humidity;
        }
        else
        {
            _temperature = 9999;
            _humidity = 9999;
        }
    }

    _lastUpdateTimems = millis();
}

float DHT11Component::getTemperature()
{
    return _temperature;
}

float DHT11Component::getHumidity()
{
    return _humidity;
}

int DHT11Component::getStatus()
{
    return _status;
}

bool DHT11Component::isStatusOk()
{
    return _status==DHTLIB_OK;
}

void DHT11Component::debug()
{
    switch (_status)
    {
    case DHTLIB_OK:
        Serial.print("OK,\t");
        break;
    case DHTLIB_ERROR_CHECKSUM:
        Serial.print("Checksum error,\t");
        break;
    case DHTLIB_ERROR_TIMEOUT:
        Serial.print("Time out error,\t");
        break;
    default:
        Serial.print("Unknown error,\t");
        break;
    }

    Serial.print(_humidity, 1);
    Serial.print(",\t");
    Serial.println(_temperature, 1);
}

void DHT11Component::onInit()
{
    updateData();
}

#endif
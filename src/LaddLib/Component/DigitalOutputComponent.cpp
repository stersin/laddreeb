#include "DigitalOutputComponent.h"

using LaddLib::Component::DigitalOutputComponent;

DigitalOutputComponent::DigitalOutputComponent(uint8_t controlPin, bool invertedMode)
        :
        _controlPin(controlPin), _invertedMode(invertedMode), _state(UNKNOWN)
{
}

bool DigitalOutputComponent::isUsable()
{
    return _controlPin > 0;
}

void DigitalOutputComponent::activate()
{
    if (!isInit())
    {
        Serial.println("ERR_ES_C_DOC_01");
        return;
    }
    else if (_state==ACTIVATED)
        return;

    if (isUsable())
        digitalWrite(_controlPin, _invertedMode ? LOW : HIGH);

    _state = ACTIVATED;
}

void DigitalOutputComponent::deactivate()
{
    if (!isInit())
    {
        Serial.println("ERR_ES_C_DOC_02");
        return;
    }
    else if (_state==DEACTIVATED)
        return;

    if (isUsable() && _state!=DEACTIVATED)
        digitalWrite(_controlPin, _invertedMode ? HIGH : LOW);

    _state = DEACTIVATED;
}

DigitalOutputComponent::State DigitalOutputComponent::getState()
{
    return _state;
}

void DigitalOutputComponent::onInit()
{
    if (isUsable())
        pinMode(_controlPin, OUTPUT);
}

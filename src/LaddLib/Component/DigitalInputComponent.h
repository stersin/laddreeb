#pragma once

#include <Arduino.h>
#include "../Initializable.h"

namespace LaddLib {
namespace Component {
class DigitalInputComponent: public Initializable {
public:
    DigitalInputComponent(uint8_t sensorPin, bool useInternalPullup = true, bool invertedMode = false);
    bool isUsable() const;
    bool hasInput() const;

private:
    uint8_t _sensorPin;
    
protected:
    virtual void onInit() override;
    bool _useInternalPullup;
    bool _invertedMode;
};
}
}

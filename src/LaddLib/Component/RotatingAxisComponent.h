#pragma once

#include <Arduino.h>
#include "./ButtonComponent.h"
#include "../Updatable.h"
#include "../Identifiable.h"

namespace LaddLib
{
  namespace Component
  {
    class RotatingAxisComponent : public Identifiable<16>, public Initializable, public Updatable
    {
    public:
      const int32_t REV_REF = 1000000;
      enum State
      {
        UNKNOWN,
        INIT,
        ROTATE,
        ROTATE_REVERSED,
        IS_ON_REF,
        IS_BETWEEN,
        ERROR
      };

      enum Direction
      {
        BACKWARD,
        FORWARD
      };

      RotatingAxisComponent(const char *id, uint8_t dirPin, uint8_t stepPin, uint32_t speedDelay, bool reverseDir = false, ButtonComponent *revSwitch = NULL);
      bool isUsable() const;
      bool isReady() const;
      void move(RotatingAxisComponent::Direction dir);
      void rotateNumRev(int32_t numRev);
      void rotate();
      void rotateReverse();
      void reset();
      bool isDestRevReached(int32_t destRev);
      Direction direction();
      State state();
      void emerg();

    protected:
      virtual void onInit() override;
      virtual void onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis) override;

      uint8_t _dirPin;
      uint8_t _stepPin;
      uint32_t _speedDelay;
      bool _reverseDir = false;
      ButtonComponent *_revSwitch;
      
      bool _isReady = false;
      bool _leftSensor = true;
      int32_t _numRev = 0;
      int32_t _destRev = 0;
      Direction _dir = Direction::BACKWARD;
      State _state = State::UNKNOWN;
    };
  }
}

#include "Pausable.h"
#include <Arduino.h>

#define LADDDEBUG_PREFIX "DEBUG:Pausable"
#define LADDDEBUG 0

using namespace LaddLib;

void Pausable::pause()
{
#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::pause");
#endif
    paused = true;
    onPause();
}

void Pausable::resume()
{
#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::resume");
#endif
    paused = false;
    onResume();
}

bool Pausable::isPaused()
{
    return paused;
}

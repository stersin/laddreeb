#include "ConveyorBeltController.h"
#include <Arduino.h>

#define LADDDEBUG_PREFIX "DEBUG:ConvBeltCtrl"
#define LADDDEBUG 0

using namespace LaddLib;

ConveyorBeltController::ConveyorBeltController(const char* id, DigitalOutputComponent beltControlRelay) :
    Identifiable(id), _beltControlRelay(beltControlRelay)
{
}

void ConveyorBeltController::onInit()
{
    _beltControlRelay.init();
    _beltControlRelay.deactivate();
}

void ConveyorBeltController::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
    if(isPaused())
        return;
}

ConveyorBeltController::State ConveyorBeltController::getState()
{
    return _state;
}

void ConveyorBeltController::startBelt()
{
#if LADDDEBUG
    {char buffer[70]; sprintf(buffer, LADDDEBUG_PREFIX":%s::startBelt", getId()); Serial.println(buffer);}
#endif
    if (_state == CONVEY)
        return;

    _beltControlRelay.activate();

    _state = CONVEY;
}

void ConveyorBeltController::stopBelt()
{
#if LADDDEBUG
    {char buffer[70]; sprintf(buffer, LADDDEBUG_PREFIX":%s::stopBelt", getId()); Serial.println(buffer);}
#endif
    if (_state != CONVEY)
        return;

    _beltControlRelay.deactivate();

    _state = IDLE;
}

void ConveyorBeltController::onPause()
{
    _beltControlRelay.deactivate();
}

void ConveyorBeltController::onResume()
{
    if (_state == CONVEY)
        _beltControlRelay.activate();
}

#pragma once

namespace LaddLib {
class Initializable {
public:
    void init();
    bool isInit() const;

protected:
    virtual void onInit() = 0;

private:
    bool _init = false;
};
}

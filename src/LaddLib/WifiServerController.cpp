#if defined(ESP8266) and defined(WIFI_ENABLE)

#include "WifiServerController.h"

#define LADDDEBUG_PREFIX "DEBUG:Es::WifiServerCtrl"
#define LADDDEBUG 0
using LaddLib::WifiServerController;

WifiServerController::WifiServerController(unsigned short port)
        :
        _wifiServer(port)
{

}

void WifiServerController::on(HttpMethod method, const char* uri, THandlerFunction handler)
{
    _handlers.push_back(new HttpRequestHandler(method, uri, handler));
}

void WifiServerController::onInit()
{
#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::onInit started");
#endif

    //_wifiServer.setNoDelay(true);
    _wifiServer.begin();

#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::onInit ended");
#endif
}

void WifiServerController::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
    // check if a client has connected
    WiFiClient client = _wifiServer.available();

    if (client)
    {
        _handleClient(client);
    }
}

void WifiServerController::_handleClient(WiFiClient& client)
{
#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::handleClientRequest New client");
#endif
    //client.setNoDelay(true);

    String content;
    unsigned long startTime = millis();
    unsigned long timeoutBeforeFirstChar = 500;
    unsigned long timeoutAfterFirstChar = 5000;

    while (client.connected() &&
            (millis()-startTime)<(content.length()>0 ? timeoutAfterFirstChar : timeoutBeforeFirstChar)
            )
    {
        int available = client.available();
        if (available>0)
        {
            char* buffer = new char[available+1];
            client.readBytes(buffer, available);
            buffer[available] = '\0';
            content += buffer;
            delete[] buffer;

            Serial.print(available);
            Serial.print('.');
            break;
        }
        else
        {
            delay(1);
        }
    }

    if (content.length()==0)
    {
        client.stop();
#if LADDDEBUG
        Serial.println(LADDDEBUG_PREFIX"::handleClientRequest Dropped timed out request");
#endif
        return;
    }

#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::handleClientRequest content=");
    Serial.println(content);
#endif

    auto request = HttpRequest(content);

#if LADDDEBUG
    Serial.println(
            String(LADDDEBUG_PREFIX"::handleClientRequest "
                    "uri=")+request.getUri()+
                    ", method="+request.getMethodString()+
                    ", query="+request.getQuery()
    );
#endif

    auto response = HttpResponse(404, "text/plain", "404 Not found");

    for (auto handler : _handlers)
    {
        if (request.getUri().compareTo(handler->getUri())==0 && handler->getMethod()==request.getMethod())
        {
            response = handler->getResponse(request);
            break;
        }
    }

    client.print(response.toString());

#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::handleClientRequest Client disconnected");
#endif
    client.stop();
}

#endif
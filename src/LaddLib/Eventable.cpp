//
//
//

#include "Eventable.h"
#include <Arduino.h>

#define LADDDEBUG_PREFIX "Debug:Eventable"
#define LADDDEBUG 0

using namespace LaddLib;

Eventable::Eventable()
{
}

void Eventable::fireEvent(const Event& evt)
{
    short evtType = evt.getType();

#if LADDDEBUG
    {char buffer[70]; sprintf(buffer, LADDDEBUG_PREFIX"::fireEvent, evtType=%d", evtType); Serial.println(buffer);}
#endif
    for (short i = 0; i < MAX_HANDLERS_PER_EVENT; i++)
    {
        if (_evtHandlers[evtType][i]) {
            (*_evtHandlers[evtType][i])(this, evt);
        }
    }
}

void Eventable::registerEventHandler(uint8_t evtType, const EventHandler& evtHandler)
{
#if LADDDEBUG
    {char buffer[70]; sprintf(buffer, LADDDEBUG_PREFIX"::registerEventHandler, evtType=%d", evtType); Serial.println(buffer);}
#endif
    for (short i = 0; i < MAX_HANDLERS_PER_EVENT; i++)
    {
        if (!_evtHandlers[evtType][i])
        {
            _evtHandlers[evtType][i] = &evtHandler;
            break;
        }
    }
}

void Eventable::unregisterEventHandler(uint8_t evtType, const EventHandler& evtHandler)
{
#if LADDDEBUG
    {char buffer[70]; sprintf(buffer, LADDDEBUG_PREFIX"::unregisterEventHandler, evtType=%d", evtType); Serial.println(buffer);}
#endif
    for (short i = 0; i < MAX_HANDLERS_PER_EVENT; i++)
    {
        if (_evtHandlers[evtType][i] == &evtHandler)
        {
            _evtHandlers[evtType][i] = 0;
        }
    }
}

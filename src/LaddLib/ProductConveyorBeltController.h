#pragma once

#include "Eventable.h"
#include "ConveyorBeltController.h"
#include "Component/DigitalInputComponent.h"

using LaddLib::Component::DigitalInputComponent;

namespace LaddLib {
class ProductConveyorBeltController:
        public ConveyorBeltController,
        public Eventable {
public:
    enum EventType {
        PRODUCTS_DELIVERED = 0
    };

    class ProductsDeliveredEvent: public LaddLib::Eventable::Event {
        friend LaddLib::ProductConveyorBeltController;
    public:
        const static EventType TYPE = EventType::PRODUCTS_DELIVERED;

        virtual uint8_t getType() const override { return TYPE; }
        unsigned short getNumProductsDelivered() const { return _numProductsDelivered; };

    private:
        unsigned short _numProductsDelivered;
    };

    ProductConveyorBeltController(const char* id, DigitalOutputComponent beltControlRelay,
            DigitalInputComponent readySensor, DigitalInputComponent deliverySensor);

    void deliverProducts(unsigned short numProductsToDeliver);
    bool isProductReady() const;
    unsigned short getNumProductsToDeliver() const;
    unsigned short getNumRemainingProductsToDeliver() const;
    unsigned short getNumProductsDelivered() const;

protected:
    virtual void onInit() override;
    virtual void onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis) override;

    virtual void onPause() override;
    virtual void onResume() override;

private:
    DigitalInputComponent _readySensor;
    DigitalInputComponent _deliverySensor;
    unsigned short _numProductsToDeliver = 0;
    unsigned short _numProductsDelivered = 0;
    bool _productDetected = false;
    ProductsDeliveredEvent _productsDeliveredEvt;
};
}

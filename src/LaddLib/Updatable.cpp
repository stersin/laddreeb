//
//
//

#include "Updatable.h"
#include <Arduino.h>

#define ESDEBUG 0

using namespace LaddLib;

Updatable::Updatable(unsigned long updateFrequency) : _updateFrequency(updateFrequency) {}

void Updatable::update()
{
    auto timems = micros();

    if(_lastUpdateTimeMicros == 0 || _lastUpdateTimeMicros > timems)
        _lastUpdateTimeMicros = micros();

    auto elapsedTime = timems - _lastUpdateTimeMicros;

    if (_updateFrequency == 0 || elapsedTime >= 1000000 / _updateFrequency)
    {
        _delayedAction.update();
        onUpdate(timems, elapsedTime);
        _lastUpdateTimeMicros = timems;
    }
}

void Updatable::delayAction(unsigned long waitTimeMicros, DelayedAction::Callback callback, void * context)
{
    _delayedAction.start(waitTimeMicros, callback, context);
}

void Updatable::DelayedAction::start(unsigned long waitTimeMicros, Callback callback, void* context)
{
    _startTimeMicros = micros();
    _waitTimeMicros = waitTimeMicros;
    _callback = callback;
    _context = context;
}

void Updatable::DelayedAction::update()
{
    if (_startTimeMicros > 0 && _startTimeMicros + _waitTimeMicros <= micros())
    {
        _startTimeMicros = _waitTimeMicros = 0;

        _callback(_context);
    }

}

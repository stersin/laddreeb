#include "AutobagController.h"

#define LADDDEBUG_PREFIX "DEBUG:AutobagCtrl"
#define LADDDEBUG 0

using namespace LaddLib;

AutobagController::AutobagController(LaddLib::Component::DigitalOutputComponent sealBagControlSwitch) :
    _sealBagControlSwitch(sealBagControlSwitch),
    _beforeSealWaitDelay(DEFAULT_BEFORE_SEAL_WAIT_DELAY),
    _afterSealWaitDelay(DEFAULT_AFTER_SEAL_WAIT_DELAY)
{
}

void AutobagController::onInit()
{
    _sealBagControlSwitch.init();
    _sealBagControlSwitch.deactivate();

    _state = IDLE;
}

void AutobagController::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
}

void AutobagController::sealBag()
{
#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::sealBag");
#endif

    if (_state != IDLE)
    {
        Serial.println("ERR_ES_AC"); //Cannot call sealBag while it is in BAG_SEAL state
        return;
    }

    _state = BAG_SEAL;
    
    delayAction(_beforeSealWaitDelay, sealBagDelayedActionCallback, this);
}

void AutobagController::setBeforeSealWaitDelay(int waitDelay)
{
    _beforeSealWaitDelay = waitDelay;
}

void AutobagController::setAfterSealWaitDelay(int waitDelay)
{
    _afterSealWaitDelay = waitDelay;
}

void AutobagController::pushBagSealSwitch()
{
    _sealBagControlSwitch.activate();
}

void AutobagController::releaseBagSealSwitch()
{
    _sealBagControlSwitch.deactivate();
}

void AutobagController::sealBagDelayedActionCallback(void* context)
{
#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::sealBagDelayedActionCallback");
#endif
    AutobagController* obj = (AutobagController*)context;

    obj->pushBagSealSwitch();

    obj->delayAction(200, obj->sealBagDelayedAction2Callback, obj);
}

void AutobagController::sealBagDelayedAction2Callback(void* context)
{
#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::sealBagDelayedAction2Callback");
#endif
    AutobagController* obj = (AutobagController*)context;

    obj->releaseBagSealSwitch();

#if LADDDEBUG
    Serial.println(String(LADDDEBUG_PREFIX"::sealBagDelayedAction2Callback Bag is sealed, waiting for another bag to be ready (") + obj->_afterSealWaitDelay + "ms delay)");
#endif
    obj->delayAction(obj->_afterSealWaitDelay, obj->sealBagDelayedAction3Callback, obj);
}

void AutobagController::sealBagDelayedAction3Callback(void* context)
{
#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::sealBagDelayedAction3Callback");
#endif
    AutobagController* obj = (AutobagController*)context;
    obj->fireEvent(obj->_bagSealedEvt);

    obj->_state = IDLE;
    
}


#pragma once

#if defined(ESP8266)

#include <ESP8266WiFi.h>
#include "EEPromUtils.h"

namespace LaddLib {

class IPAddressFix: public IPAddress {
public:
    String toString() const
    {
        char szRet[16];
        sprintf(szRet, "%u.%u.%u.%u", (*this)[0], (*this)[1], (*this)[2], (*this)[3]);
        return String(szRet);
    }
};

class WifiConfig {
public:
    WifiConfig()
    {

    }

    WifiConfig(
            const char* ssid, const char* password,
            const IPAddress& ip, const IPAddress& gateway, const IPAddress& networkMask,
            const WiFiMode_t& mode
    )
            :_ip((const IPAddressFix&) ip), _gateway((const IPAddressFix&) gateway),
             _networkMask((const IPAddressFix&) networkMask), _mode(mode)
    {
        setSsid(ssid);
        setPassword(password);
    }

    static const uint8_t SSID_MAX_LENGTH = WL_SSID_MAX_LENGTH;
    static const uint8_t PASSWORD_MAX_LENGTH = 32;

    const String getSsidAsString() const { return _ssid; }
    const char* getSsid() const { return _ssid; }
    const String getPasswordAsString() const { return _password; }
    const char* getPassword() const { return _password; }
    const IPAddressFix& getIp() const { return _ip; }
    const String getIpForInput() const { return (uint32_t) _ip==0 ? "" : _ip.toString(); }
    const IPAddressFix& getNetworkMask() const { return _networkMask; }
    const IPAddressFix& getGateway() const { return _gateway; }
    const WiFiMode_t& getMode() const { return _mode; }
    String getModeAsString() const
    {
        switch (_mode)
        {
        case WIFI_STA:
            return "station";
        case WIFI_AP:
            return "accesspoint";
        case WIFI_AP_STA:
            return "station+accesspoint";
        case WIFI_OFF:
            return "offline";
        }
    }

    void setSsid(const char* ssid)
    {
        int len = strlen(ssid);
        if (len>SSID_MAX_LENGTH-1) len = SSID_MAX_LENGTH-1;
        memcpy(_ssid, ssid, len);
        _ssid[len] = '\0';
    };
    void setSsid(const String& ssid) { ssid.toCharArray(_ssid, SSID_MAX_LENGTH); }
    void setPassword(const char* password)
    {
        int len = strlen(password);
        if (len>PASSWORD_MAX_LENGTH-1) len = PASSWORD_MAX_LENGTH-1;
        memcpy(_password, password, len);
        _password[len] = '\0';
    };
    void setPassword(const String& password) { password.toCharArray(_password, PASSWORD_MAX_LENGTH); }
    void setIp(const IPAddressFix& ip) { _ip = ip; }
    void setIp(const String& ip) { _ip.fromString(ip); }
    void setNetworkMask(const IPAddressFix& networkMask) { _networkMask = networkMask; }
    void setNetworkMask(const String& networkMask) { _networkMask.fromString(networkMask); }
    void setGateway(const IPAddressFix& gateway) { _gateway = gateway; }
    void setGateway(const String& gateway) { _gateway.fromString(gateway); }
    void setMode(const WiFiMode_t& mode) { _mode = mode; }

    const String toString() const
    {
        String s = "mode="+getModeAsString()
                +", ssid="+getSsid()
                +", password="+getPassword()
                +", ip="+getIp().toString()
                +", gateway="+getGateway().toString()
                +", networkMask="+getNetworkMask().toString();
        return s;
    }
private:
    char _ssid[SSID_MAX_LENGTH+1];
    char _password[PASSWORD_MAX_LENGTH+1];
    IPAddressFix _ip;
    IPAddressFix _networkMask;
    IPAddressFix _gateway;
    WiFiMode_t _mode;
};

class EEPromWifiConfigManager {
public:
    EEPromWifiConfigManager(int location, WifiConfig& config)
            :_location(location), _config(config)
    {

    }

    void load()
    {
        Serial.println("EPROMConfig load start");
        EEPROM.begin(E2END);
        EEPROM_safe_read(0, (uint8_t*) &_config, sizeof(WifiConfig));
        EEPROM.end();
        Serial.println("EPROMConfig load end");

    }

    void save()
    {
        Serial.println("EPROMConfig save start");
        EEPROM.begin(E2END);
        EEPROM_safe_write(0, (uint8_t*) &_config, sizeof(WifiConfig));
        EEPROM.commit();
        EEPROM.end();
        Serial.println("EPROMConfig save end");
    }
private:
    int _location;
    WifiConfig& _config;
};
}

#endif
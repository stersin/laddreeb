#pragma once

namespace LaddLib {
class Updatable {
public:
    static const unsigned long DEFAULT_UPDATE_FREQUENCY_HZ = 0;

    Updatable(unsigned long updateFrequency = DEFAULT_UPDATE_FREQUENCY_HZ);

    void update();

protected:
    class DelayedAction {
    public:
        typedef void(* Callback)(void* context);
        void start(unsigned long waitTimeMicros, Callback callback, void* context);
        void update();

    private:
        Callback _callback = 0;
        void* _context = 0;
        unsigned long _startTimeMicros = 0;
        unsigned long _waitTimeMicros = 0;
    };

    virtual void onUpdate(unsigned long timeMicros, unsigned long elapsedTimeMicros) = 0;

    void delayAction(unsigned long waitTimeMicros, DelayedAction::Callback callback, void* context);

private:
    unsigned long _updateFrequency;
    unsigned long _lastUpdateTimeMicros;
    DelayedAction _delayedAction;

};
}

#pragma once
#if defined(ESP8266) and defined(EEPROM_ENABLE)

#include <Arduino.h>

namespace LaddLib {
namespace Helper {
class Eeprom {
public:
    static void read(int location, uint8_t* buffer, int bytesToRead);
    static void write(int location, uint8_t* buffer, int bufferLength);
};
}
}

#endif
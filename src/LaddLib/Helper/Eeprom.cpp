#include "Eeprom.h"

#if defined(ESP8266) and defined(EEPROM_ENABLE)

#define LADDDEBUG_PREFIX "DEBUG:Es::He::Eeprom"
#define LADDDEBUG 0

using namespace LaddLib::Helper;

void Eeprom::read(int location, uint8_t* buffer, int bytesToRead)
{
#if LADDDEBUG
    Serial.println("EPROMConfig read start");
#endif
#if defined(ESP8266)
    EEPROM.begin(E2END);
        EEPROM_safe_read(location, buffer, bytesToRead);
        EEPROM.end();
#else
    EEPROM_safe_read(location, buffer, bytesToRead);
#endif

#if LADDDEBUG
    Serial.println("EPROMConfig read end");
#endif
}

void Eeprom::write(int location, uint8_t* buffer, int bufferLength)
{
#if LADDDEBUG
    Serial.println("EPROMConfig save start");
#endif
#if defined(ESP8266)
    EEPROM.begin(E2END);
        EEPROM_safe_write(location, buffer, bufferLength);
        EEPROM.commit();
        EEPROM.end();
#else
    EEPROM_safe_write(location, buffer, bufferLength);
#endif
#if LADDDEBUG
    Serial.println("EPROMConfig save end");
#endif
}

#endif
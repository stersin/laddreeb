#include "ProductConveyorBeltController.h"

#define LADDDEBUG_PREFIX "DEBUG:ProdConvBeltCtrl"
#define LADDDEBUG 0

using namespace LaddLib;

using LaddLib::Component::DigitalInputComponent;

ProductConveyorBeltController::ProductConveyorBeltController(const char* id, DigitalOutputComponent beltControlRelay, DigitalInputComponent readySensor, DigitalInputComponent deliverySensor)
    : ConveyorBeltController(id, beltControlRelay),
    _readySensor(readySensor),
    _deliverySensor(deliverySensor)
{
}

void ProductConveyorBeltController::deliverProducts(unsigned short numProductsToDeliver)
{
    if (_numProductsToDeliver != 0)
    {
        char* buffer = new char[100]; sprintf(buffer, "ERR_ES_PCBC:%s Cannot call deliverProducts when there are remaining products to deliver", getId()); Serial.println(buffer); delete buffer;
        return;
    }

    _numProductsToDeliver = numProductsToDeliver;
}

bool ProductConveyorBeltController::isProductReady() const
{
    return _readySensor.hasInput();
}


unsigned short ProductConveyorBeltController::getNumProductsToDeliver() const
{
    return _numProductsToDeliver;
}

unsigned short ProductConveyorBeltController::getNumRemainingProductsToDeliver() const
{
    return _numProductsToDeliver - _numProductsDelivered;
}

unsigned short ProductConveyorBeltController::getNumProductsDelivered() const
{
    return _numProductsDelivered;
}

void ProductConveyorBeltController::onInit()
{
    ConveyorBeltController::onInit();

    _readySensor.init();
    _deliverySensor.init();
}

void ProductConveyorBeltController::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
    ConveyorBeltController::onUpdate(timeMillis, elapsedTimeMillis);

    if(isPaused())
        return;
        
    auto prodReady = isProductReady();
    auto remainProd = getNumRemainingProductsToDeliver() > 0;
    auto conveying = getState() == CONVEY;

    // when a products is ready and there is no more product to deliver, the belt should be stopped (if not already stopped)
    if (
        prodReady && !remainProd && conveying
        )
    {
    #if LADDDEBUG
        char* buffer = new char[100]; sprintf(buffer, LADDDEBUG_PREFIX":%s: Product is ready, stopping belt", getId()); Serial.println(buffer); delete buffer;
    #endif
        stopBelt();
    }
    // else the belt should be started (if not already started)
    else if ((remainProd || !prodReady) && !conveying)
    {
    #if LADDDEBUG
        if (getNumRemainingProductsToDeliver() > 0)
        {
            char* buffer = new char[100]; sprintf(buffer, LADDDEBUG_PREFIX":%s: Product is to be delivered, starting belt", getId()); Serial.println(buffer); delete buffer;
        }
        else
        {
            char* buffer = new char[100]; sprintf(buffer, LADDDEBUG_PREFIX":%s: Product is not ready, starting belt", getId()); Serial.println(buffer); delete buffer;
        }
    #endif
        startBelt();
    }

    if (remainProd)
    {
        // manage product delivery detection
        short pinStateHigh = _deliverySensor.isUsable() ? _deliverySensor.hasInput() : _readySensor.hasInput();

        // product first enters light sensor beam
        if (pinStateHigh && !_productDetected)
        {
        #if LADDDEBUG
            char* buffer = new char[100]; sprintf(buffer, "%s: Product is detected", getId()); Serial.println(buffer); delete buffer;
        #endif
            _productDetected = true;
        }
        // product exits light sensor beam
        else if (!pinStateHigh && _productDetected)
        {
            _numProductsDelivered++;
            _productDetected = false;

            char* buffer = new char[100]; sprintf(buffer, "%s: Product is delivered (%d/%d)", getId(), _numProductsDelivered, _numProductsToDeliver); Serial.println(buffer); delete buffer;

            if (_numProductsDelivered == _numProductsToDeliver)
            {
                _productsDeliveredEvt._numProductsDelivered = _numProductsDelivered;
                _numProductsDelivered = _numProductsToDeliver = 0;

                fireEvent(_productsDeliveredEvt);
            }
        }
    }
}

void ProductConveyorBeltController::onPause()
{
    ConveyorBeltController::onPause();
}

void ProductConveyorBeltController::onResume()
{
    ConveyorBeltController::onResume();
}

#pragma once

#if defined(ESP8266) and defined(WIFI_ENABLE)

#include "Initializable.h"
#include "Updatable.h"
#include "Eventable.h"
#include "WifiController.h"
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <functional>
#include <vector>

namespace LaddLib {
class WifiServerController:
        public Initializable,
        public Updatable,
        public Eventable {
public:
    enum EventType {
        REQUEST = 0
    };

    enum HttpMethod { HTTP_GET, HTTP_POST, HTTP_PUT, HTTP_PATCH, HTTP_DELETE, HTTP_OPTIONS };

    class HttpRequest {
    public:
        HttpRequest(String content)
                :_rawContent(content)
        {
            auto req = _rawContent.substring(0, _rawContent.indexOf('\r'));

            // First line of HTTP request looks like "GET /path HTTP/1.1"
            // Retrieve the "/path" part by finding the spaces
            int addrStart = req.indexOf(' ');
            int addrEnd = req.indexOf(' ', addrStart+1);
            /*if (addrStart==-1 || addrEnd==-1)
            {
                return false;
            }*/

            String methodStr = req.substring(0, addrStart);
            _uri = req.substring(addrStart+1, addrEnd);
            _query = "";
            int hasQuery = _uri.indexOf('?');
            if (hasQuery!=-1)
            {
                _query = String("&")+_uri.substring(hasQuery+1);
                _uri = _uri.substring(0, hasQuery);
            }

            _method = HTTP_GET;
            if (methodStr=="POST")
            {
                _method = HTTP_POST;
            }
            else if (methodStr=="DELETE")
            {
                _method = HTTP_DELETE;
            }
            else if (methodStr=="OPTIONS")
            {
                _method = HTTP_OPTIONS;
            }
            else if (methodStr=="PUT")
            {
                _method = HTTP_PUT;
            }
            else if (methodStr=="PATCH")
            {
                _method = HTTP_PATCH;
            }
        }

        String getUri() const { return _uri; }

        HttpMethod getMethod() const { return _method; }

        const char* getMethodString() const
        {
            switch (_method)
            {
            case HTTP_GET:
                return "GET";
            case HTTP_DELETE:
                return "DELETE";
            case HTTP_OPTIONS:
                return "OPTIONS";
            case HTTP_PATCH:
                return "PATCH";
            case HTTP_POST:
                return "POST";
            case HTTP_PUT:
                return "PUT";
            }
        }

        String getQuery() const { return _query; }

        String getStringArgValue(const char* argName, const char* defaultValue = "") const
        {
            auto search = String("&")+argName+"=";
            auto pos = _query.indexOf(search);
            if (pos!=-1)
            {
                return ESP8266WebServer::urlDecode(
                        _query.substring(pos+search.length(), _query.indexOf('&', pos+1)));
            }
            else
            {
                return defaultValue;
            }
        }
    private:
        String _rawContent;
        String _uri;
        String _query;
        HttpMethod _method;
    };

    class HttpResponse {
    public:
        HttpResponse(uint16_t code, const char* contentType, String body, String additionalHeaders)
                :_code(code), _contentType(contentType), _body(body), _additionalHeaders(additionalHeaders)
        {
        }

        HttpResponse(uint16_t code, const char* contentType, String body)
                :HttpResponse(code, contentType, body, "")
        {
        }

        HttpResponse(uint16_t code, String body)
                :HttpResponse(code, "application/json", body, "")
        {
        }
        uint16_t getCode() const { return _code; };
        const char* getContentType() const { return _contentType; };
        String getBody() const { return _body; };
        String appendHeader(String header)
        {
            _additionalHeaders += (_additionalHeaders.length()>0 ? "\n" : "")+header;
        }
        String toString() const
        {

            String responseStatus = "";
            String responseBody = getBody();
            String responseContentType = getContentType();
            uint16_t responseCode = getCode();

            switch (responseCode)
            {
            case 200:
                responseStatus += String(responseCode)+" OK";
                break;
            case 301:
                responseStatus += String(responseCode)+" Moved Permanently";
                responseBody = "";
                break;
            case 302:
                responseStatus += String(responseCode)+" Found";
                responseBody = "";
                break;
            case 303:
                responseStatus += String(responseCode)+" See Other";
                responseBody = "";
                break;
            case 400:
                responseStatus += String(responseCode)+" Bad Request";
                break;
            case 404:
                responseStatus += String(responseCode)+" Not Found";
                responseBody = "\"Not found\"";
                break;
            default:
                responseStatus += "500 Internal Server Error";
                responseBody = "";
            }

#if LADDDEBUG
            Serial.println(
            String(LADDDEBUG_PREFIX"::handleClientRequest Response code=")+responseCode);
#endif

            String response = String("HTTP/1.1 ")+responseStatus+"\n";
            if(strlen(_contentType) > 0)
                response += String("Content-Type: ")+responseContentType+"\n";
            if(_additionalHeaders.length() > 0)
                response += _additionalHeaders+"\n";
            response += "\n" + responseBody;

            return response;
        }
        HttpResponse& setNoCache()
        {
            appendHeader("Cache-Control: no-cache");

            return *this;
        }
    private:
        uint16_t _code;
        const char* _contentType;
        String _additionalHeaders;
        String _body;
    };

    class HttpRedirectResponse: public HttpResponse {
    public:
        HttpRedirectResponse(bool permanent, String location, String additionalHeaders)
                : HttpResponse(permanent ? 301 : 303, "", "", additionalHeaders)
        {
            appendHeader(String("Location: ")+location);
        }

        HttpRedirectResponse(bool permanent, String location)
                :
                HttpRedirectResponse(permanent, location, "")
        {
        }
    };

    typedef std::function<HttpResponse(const HttpRequest&)> THandlerFunction;

    class HttpRequestHandler {
    public:
        HttpRequestHandler(HttpMethod method, const char* uri, THandlerFunction handler)
                :_method(method), _uri(uri), _handler(handler)
        {

        }

        HttpMethod getMethod() const { return _method; }
        const char* getUri() const { return _uri; }
        HttpResponse getResponse(const HttpRequest& request) const { return _handler(request); }
    private:
        HttpMethod _method;
        const char* _uri;
        THandlerFunction _handler;
    };

    WifiServerController(unsigned short port);

    void on(HttpMethod method, const char* uri, THandlerFunction fn);

protected:
    virtual void onInit() override;
    virtual void onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis) override;

private:
    WiFiServer _wifiServer;
    std::vector<HttpRequestHandler*> _handlers;

    void _handleClient(WiFiClient& client);

};
}

#endif
#pragma once

#if defined(ESP8266) and defined(WIFI_ENABLE)

#include "Initializable.h"
#include "Updatable.h"
#include "Eventable.h"
#include "WifiConfig.h"
#include "WifiController.h"
#include "WifiServerController.h"

namespace LaddLib {
class WifiConfigurationController:
        public Initializable,
        public Updatable,
        public Eventable {
public:
    const static int EEPROM_LOCATION = 0x10;
    WifiConfigurationController(
            WifiController& wifiController, WifiServerController& wifiServerController);

    void reset();
protected:
    virtual void onInit() override;
    virtual void onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis) override;

private:
    WifiConfig _defaultConfig;
    WifiController& _wifiController;
    WifiServerController& _wifiServerController;

    LaddLib::WifiServerController::HttpResponse _onConfigWebRequest(const LaddLib::WifiServerController::HttpRequest& request);
    LaddLib::WifiServerController::HttpResponse _onConfigSaveWebRequest(const LaddLib::WifiServerController::HttpRequest& request);
    LaddLib::WifiServerController::HttpResponse _onConfigResetWebRequest(const LaddLib::WifiServerController::HttpRequest& request);
};
}

#endif
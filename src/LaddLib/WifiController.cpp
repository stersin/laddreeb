#if defined(ESP8266) and defined(WIFI_ENABLE)

#include "WifiController.h"

#define LADDDEBUG_PREFIX "DEBUG:Es::WifiCtrl"
#define LADDDEBUG 0

using LaddLib::WifiController;

WifiController::StatusChangedEvent::StatusChangedEvent(wl_status_t status, wl_status_t fromStatus)
        :_status(status), _fromStatus(fromStatus)
{

}

uint8_t WifiController::StatusChangedEvent::getType() const
{
    return TYPE;
}

wl_status_t WifiController::StatusChangedEvent::getStatus() const
{
    return _status;
}

wl_status_t WifiController::StatusChangedEvent::getFromStatus() const
{
    return _fromStatus;
}

WifiController::WifiController(const LaddLib::WifiConfig& config)
        :
        _config(config)
{

}

const char* WifiController::getStatusString() const
{
    switch (WiFi.status())
    {
    case WL_NO_SHIELD:
        return "WL_NO_SHIELD";
    case WL_IDLE_STATUS:
        return "WL_IDLE_STATUS";
    case WL_NO_SSID_AVAIL:
        return "WL_NO_SSID_AVAIL";
    case WL_SCAN_COMPLETED:
        return "WL_SCAN_COMPLETED";
    case WL_CONNECTED:
        return "WL_CONNECTED";
    case WL_CONNECT_FAILED:
        return "WL_CONNECT_FAILED";
    case WL_CONNECTION_LOST:
        return "WL_CONNECTION_LOST";
    case WL_DISCONNECTED:
        return "WL_DISCONNECTED";
    }
}

const LaddLib::WifiConfig& WifiController::getConfig() const
{
    return _config;
}

void WifiController::setConfig(const WifiConfig& config, bool reconnect)
{
    _config = config;

    if (reconnect)
    {
        _disconnect();
        _reconnect = true;
    }
}

void WifiController::connect()
{
    _reconnect = true;
}

void WifiController::disconnect()
{
    _reconnect = false;
    _disconnect();
}

void WifiController::onInit()
{
}

void WifiController::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
    bool stationMode = WiFi.getMode()==WiFiMode::WIFI_STA;
    bool wasConnected = _isConnected;

    if (stationMode && wasConnected && WiFi.isConnected())
    {
        // handle network disconnection case on STA mode
#if LADDDEBUG
        Serial.println(String(LADDDEBUG_PREFIX"::onUpdate Disconnected, status=")+getStatusString());
#endif
        _isConnected = _isConnecting = false;
    }

    if(!wasConnected && !_isConnecting && _reconnect)
    {
        _connect();
    }
}

void WifiController::_connect()
{
    if(_isConnected) {
        return;
    }

    WiFi.mode(_config.getMode());

    if (_config.getMode()==WIFI_STA)
    {
#if LADDDEBUG
        Serial.println(
                String(LADDDEBUG_PREFIX"::_connect mode=WIFI_STA")
                        +", ssid="+_config.getSsid()
                        +", password="+_config.getPassword()
                        +", ip="+_config.getIp().toString()
                        +", gateway="+_config.getGateway().toString()
                        +", networkMask="+_config.getNetworkMask().toString()
        );
#endif
        WiFi.config(_config.getIp(), _config.getGateway(), _config.getNetworkMask());
        WiFi.begin(_config.getSsid(), _config.getPassword());

        _isConnected = false;
        _isConnecting = true;
    }
    else
    {

#if LADDDEBUG
        Serial.println(
                String(LADDDEBUG_PREFIX"::_connect mode=WIFI_AP")
                        +", ssid="+_config.getSsid()
                        +", password="+_config.getPassword()
                        +", ip="+_config.getIp().toString()
                        +", gateway="+_config.getGateway().toString()
                        +", networkMask="+_config.getNetworkMask().toString()
        );
#endif
        WiFi.softAPConfig(_config.getIp(), _config.getGateway(), _config.getNetworkMask());

        if (!WiFi.softAP(_config.getSsid(), _config.getPassword()))
        {
            _isConnected = _isConnecting = false;
            Serial.println("ERR_ES_WC_ERR_01");
            return;
        }

        // at this point we consider to be connected on AP mode
        _isConnected = true;
        _isConnecting = false;
    }
}

void WifiController::_disconnect()
{
    if(_isConnected)
    {
        if(WiFi.getMode() == WiFiMode::WIFI_AP)
        {
#if LADDDEBUG
            Serial.println(LADDDEBUG_PREFIX"::_disconnect WiFi.softAPdisconnect()");
#endif
            WiFi.softAPdisconnect(true);
        }
        else
        {
#if LADDDEBUG
            Serial.println(LADDDEBUG_PREFIX"::_disconnect WiFi.disconnect()");
#endif
            WiFi.disconnect(true);
        }

        _isConnected = _isConnecting = false;
    }
}

#endif
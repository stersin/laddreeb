#pragma once

#if defined(ESP8266) and defined(WIFI_ENABLE)

#include "Initializable.h"
#include "Updatable.h"
#include "Eventable.h"
#include "WifiConfig.h"

namespace LaddLib {
class WifiController:
        public Initializable,
        public Updatable,
        public Eventable {
public:
    enum EventType {
        STATUS_CHANGED = 0
    };

    class StatusChangedEvent: public Event {
    public:
        StatusChangedEvent(wl_status_t status, wl_status_t fromStatus);
        static const EventType TYPE = EventType::STATUS_CHANGED;

        virtual uint8_t getType() const override;
        wl_status_t getStatus() const;
        wl_status_t getFromStatus() const;

    private:
        wl_status_t _status;
        wl_status_t _fromStatus;
    };

    WifiController(const LaddLib::WifiConfig& wifiConfig);
    const char* getStatusString() const;
    const wl_status_t getStatus() const;
    const WifiConfig& getConfig() const;
    void setConfig(const WifiConfig& config, bool reconnect = true);

    void connect();
    void disconnect();

protected:
    virtual void onInit() override;
    virtual void onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis) override;

private:
    /**
     * Whether or not reconnect as soon as possible
     */
    bool _reconnect = true;

    /**
     * Whether or not the last state was connected
     */
    bool _isConnected = false;

    /**
     * Whether or not a connection attempt was launched previously
     */
    bool _isConnecting = false;

    /**
     * To apply wifi config
     */
    WifiConfig _config;

    void _disconnect();
    void _connect();
};
}

#endif
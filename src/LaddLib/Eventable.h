#pragma once

#include <Arduino.h>

namespace LaddLib {
class Eventable {
public:
    class Event {
    public:
        virtual uint8_t getType() const = 0;
    };

    class EventHandler {
    public:
        typedef void(* Callback)(const Event& evt, Eventable* issuer, void* context);
        EventHandler(Callback callback, void* context)
                :_callback(callback), _context(context) { }
        void operator()(Eventable* issuer, const Event& evt) const { _callback(evt, issuer, _context); }
    private:
        Callback _callback;
        void* _context;
    };

    Eventable();
    void fireEvent(const Event& evt);
    void registerEventHandler(uint8_t evtType, const EventHandler& evtHandler);
    void unregisterEventHandler(uint8_t evtType, const EventHandler& evtHandler);

private:
    static const uint8_t MAX_EVENTS = 3;
    static const uint8_t MAX_HANDLERS_PER_EVENT = 2;

    const EventHandler* _evtHandlers[MAX_EVENTS][MAX_HANDLERS_PER_EVENT] = {{0}};
};
}

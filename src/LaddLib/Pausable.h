#pragma once

namespace LaddLib {

class Pausable {
public:
    void pause();
    void resume();

    bool isPaused();

protected:
    virtual void onPause() = 0;
    virtual void onResume() = 0;

private:
    bool paused = false;
};
}

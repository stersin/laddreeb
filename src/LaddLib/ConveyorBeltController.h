#pragma once

#include "Identifiable.h"
#include "Initializable.h"
#include "Updatable.h"
#include "Pausable.h"
#include "Component/DigitalOutputComponent.h"

using LaddLib::Component::DigitalOutputComponent;

namespace LaddLib {
class ConveyorBeltController:
        public Identifiable<16>,
        public Initializable,
        public Updatable,
        public Pausable {
public:
    enum State {
        INIT,
        IDLE,
        CONVEY
    };

    ConveyorBeltController(const char* id, DigitalOutputComponent beltControlRelay);

    State getState();

    void startBelt();
    void stopBelt();

protected:
    virtual void onInit() override;
    virtual void onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis) override;

    virtual void onPause() override;
    virtual void onResume() override;

private:
    State _state = INIT;
    DigitalOutputComponent _beltControlRelay;
};
}

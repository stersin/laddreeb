#pragma once

#include <Arduino.h>

namespace LaddLib {
template<uint8_t LENGTH>
class Identifiable {
public:
    Identifiable(const char* id)
    {
        size_t len = strlen(id);
        memcpy(_id, id, len<LENGTH ? len : LENGTH);
    }

    const char* getId() const
    {
        return _id;
    }

protected:
    char _id[LENGTH] = {0};
};
}

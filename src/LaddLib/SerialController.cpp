#include "SerialController.h"
#include <Arduino.h>

#define LADDDEBUG_PREFIX "DEBUG:Es::SerialCtrl"
#define LADDDEBUG 0

using namespace LaddLib;

SerialController::SerialController(unsigned long baudrate)
        :_baudrate(baudrate), Updatable(baudrate/8) { }

SerialController::NewLineEvent::NewLineEvent(const char* line)
        :_line(line)
{

}

uint8_t SerialController::NewLineEvent::getType() const
{
    return TYPE;
}

const char* SerialController::NewLineEvent::getLine() const
{
    return _line;
}

void SerialController::onInit()
{
#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::onInit started");
#endif

    Serial.begin(_baudrate);

    while (!Serial)
    {
        delay(100); // wait for serial port to connect. Needed for native USB port only
    }

#if LADDDEBUG
    Serial.println(LADDDEBUG_PREFIX"::onInit ended");
#endif
}

void SerialController::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
    if (Serial.available())
    {
        char byte = Serial.read();

        switch (byte)
        {
        case '\n':
            // terminating string with null byte
            _inputLine[_inputPos] = 0;

            if (_inputPos>0)
            {
#if LADDDEBUG
                {
                    char buffer[70];
                    sprintf(buffer, LADDDEBUG_PREFIX"::onUpdate New line : %s", _inputLine);
                    Serial.println(buffer);
                }
#endif
                fireEvent(NewLineEvent(_inputLine));
            }

            _inputPos = 0;
            break;
        case '\r':
            // discard carriage return
            break;

        default:
            // add char keeping space for terminating null byte
            if (_inputPos<MAX_LINE_LENGTH-1)
                _inputLine[_inputPos++] = byte;
            else
            {
                Serial.println("ERR_ES_SC_01");
                _inputPos = 0;
            }
        }
    }
}

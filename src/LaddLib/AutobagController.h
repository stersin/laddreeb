#pragma once

#include "Initializable.h"
#include "Updatable.h"
#include "Eventable.h"
#include "Pausable.h"
#include "Component/DigitalOutputComponent.h"

using LaddLib::Component::DigitalOutputComponent;

namespace LaddLib {
class AutobagController:
        public Initializable,
        public Updatable,
        public Eventable {
public:
    const int DEFAULT_BEFORE_SEAL_WAIT_DELAY = 1000;
    const int DEFAULT_AFTER_SEAL_WAIT_DELAY = 5000;

    enum EventType {
        BAG_SEALED = 0
    };

    class BagSealedEvent: public Event {
    public:
        const static EventType TYPE = EventType::BAG_SEALED;

        virtual uint8_t getType() const override { return TYPE; }
    };

    enum State {
        INIT,
        IDLE,
        BAG_SEAL
    };

    AutobagController(DigitalOutputComponent sealBagControlSwitch);

    void sealBag();

    void setBeforeSealWaitDelay(int waitDelay);
    void setAfterSealWaitDelay(int waitDelay);

protected:
    virtual void onInit() override;
    virtual void onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis);

private:
    State _state = INIT;
    DigitalOutputComponent _sealBagControlSwitch;
    int _beforeSealWaitDelay;
    int _afterSealWaitDelay;
    BagSealedEvent _bagSealedEvt;

    void pushBagSealSwitch();
    void releaseBagSealSwitch();
    static void sealBagDelayedActionCallback(void* context);
    static void sealBagDelayedAction2Callback(void* context);
    static void sealBagDelayedAction3Callback(void* context);
};
}



#include "WifiConfigurationController.h"

#if defined(ESP8266) and defined(WIFI_ENABLE)

#include <functional>

#define LADDDEBUG_PREFIX "DEBUG:Es::WifiConfCtrl"
#define LADDDEBUG 0

using LaddLib::WifiConfigurationController;

WifiConfigurationController::WifiConfigurationController(WifiController& wifiController,
        WifiServerController& wifiServerController)
        :
        _wifiController(wifiController),
        _wifiServerController(wifiServerController)
{
    _defaultConfig = _wifiController.getConfig();
}

void WifiConfigurationController::reset()
{
    EEPromWifiConfigManager manager(EEPROM_LOCATION, _defaultConfig);
    manager.save();
    _wifiController.setConfig(_defaultConfig);
}

void WifiConfigurationController::onInit()
{
    WifiConfig config = _wifiController.getConfig();
    EEPromWifiConfigManager manager(EEPROM_LOCATION, config);
    manager.load();
    _wifiController.setConfig(config);

    _wifiServerController.on(WifiServerController::HttpMethod::HTTP_GET, "/wificonfig",
            std::bind(&WifiConfigurationController::_onConfigWebRequest, this, std::placeholders::_1));
    _wifiServerController.on(WifiServerController::HttpMethod::HTTP_GET, "/wificonfig/save",
            std::bind(&WifiConfigurationController::_onConfigSaveWebRequest, this, std::placeholders::_1));
    _wifiServerController.on(WifiServerController::HttpMethod::HTTP_GET, "/wificonfig/reset",
            std::bind(&WifiConfigurationController::_onConfigResetWebRequest, this, std::placeholders::_1));
}

void WifiConfigurationController::onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis)
{
}

LaddLib::WifiServerController::HttpResponse WifiConfigurationController::_onConfigWebRequest(
        const LaddLib::WifiServerController::HttpRequest& request)
{
    Serial.println(LADDDEBUG_PREFIX"::_onConfigWebRequest");

    WifiConfig config = _wifiController.getConfig();
    EEPromWifiConfigManager manager(EEPROM_LOCATION, config);
    manager.load();

    auto body = String("<!DOCTYPE HTML>")+
            "<html>"
                    "<head>"
                    "<title>Megabox scale - Wifi configuration</title>"
                    "</head>"
                    "<body>"
                    "<style type='text/css'>"
                    "label{display:block; float:left; width:150px;}"
                    "</style>"
                    "<h1>Megabox scale - Wifi configuration</h1>"
                    "<form method='get' action='/wificonfig/save'>"
                    "<p>"
                    "<label for='mode'>Mode:</label>"
                    "<select name='mode'>"
                    "<option value='1'"+(config.getMode()==WIFI_STA ? " selected='selected'" : "")+">Station</option>"
            "<option value='2'"+(config.getMode()==WIFI_AP ? " selected='selected'" : "")+">Access point</option>"
            "</select>"
            "</p>"
            "<p><label for='ssid'>SSID:</label><input name='ssid' value='"+config.getSsid()+"' length='32'></p>"
            "<p><label for='password'>Password:</label><input name='password' value='"+config.getPassword()
            +"' length='32'></p>"
                    "<p><label for='ip'>IP:</label><input name='ip' value='"+config.getIp().toString()
            +"' length='15'></p>"
                    "<p><label for='mask'>Network mask:</label><input name='mask' value='"
            +config.getNetworkMask().toString()+"' length='15'></p>"
            "<p><label for='gateway'>Gateway:</label><input name='gateway' value='"+config.getGateway().toString()
            +"' length='15'></p>"
                    "<p><input type='submit'></p>"
                    "</form>"
                    "</body>"
                    "</html>";

    return LaddLib::WifiServerController::HttpResponse(200, "text/html", body);
}

LaddLib::WifiServerController::HttpResponse WifiConfigurationController::_onConfigSaveWebRequest(
        const LaddLib::WifiServerController::HttpRequest& request)
{
    Serial.println(LADDDEBUG_PREFIX"::_onConfigSaveWebRequest");

    WifiConfig config = _wifiController.getConfig();
    EEPromWifiConfigManager manager(EEPROM_LOCATION, config);
    manager.load();
    config.setMode((WiFiMode_t) request.getStringArgValue("mode").toInt());
    config.setSsid(request.getStringArgValue("ssid"));
    config.setPassword(request.getStringArgValue("password"));
    config.setIp(request.getStringArgValue("ip"));
    config.setNetworkMask(request.getStringArgValue("network_mask"));
    config.setGateway(request.getStringArgValue("gateway"));
    manager.save();

    _wifiController.setConfig(config);

    return LaddLib::WifiServerController::HttpRedirectResponse(false, "/wificonfig?message=config_saved");
}

LaddLib::WifiServerController::HttpResponse WifiConfigurationController::_onConfigResetWebRequest(
        const LaddLib::WifiServerController::HttpRequest& request)
{
    Serial.println(LADDDEBUG_PREFIX"::_onConfigResetWebRequest");

    WifiConfig config = _wifiController.getConfig();
    EEPromWifiConfigManager manager(EEPROM_LOCATION, config);
    manager.load();

    return LaddLib::WifiServerController::HttpRedirectResponse(false, "/wificonfig?message=config_reset");
}

#endif
#pragma once

#include "Initializable.h"
#include "Updatable.h"
#include "Eventable.h"

namespace LaddLib {
class SerialController:
        public Initializable,
        public Updatable,
        public Eventable {
public:
    static const unsigned long DEFAULT_BAUDRATE = (unsigned long) 115200;
    static const uint8_t MAX_LINE_LENGTH = 30;

    enum EventType {
        NEW_LINE = 0
    };

    class NewLineEvent: public Event {
    public:
        NewLineEvent(const char* line);
        static const EventType TYPE = EventType::NEW_LINE;

        virtual uint8_t getType() const override;

        const char* getLine() const;

    private:
        const char* _line;
    };

    SerialController(unsigned long baudrate = DEFAULT_BAUDRATE);

protected:
    virtual void onInit() override;
    virtual void onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis) override;

private:
    unsigned long _baudrate;
    char _inputLine[MAX_LINE_LENGTH];
    uint8_t _inputPos = 0;
};
}

#include "Initializable.h"
#include <Arduino.h>

#define LADDDEBUG_PREFIX "DEBUG:Initializable"
#define LADDDEBUG 0

using namespace LaddLib;

void Initializable::init()
{
    #if LADDDEBUG
        Serial.println(LADDDEBUG_PREFIX"::init");
    #endif

    onInit();

    _init = true;
}

bool Initializable::isInit() const
{
    return _init;
}

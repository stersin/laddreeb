#include "MasterController.h"

MasterController masterCtrl;

void setup()
{
    delay(3000);
    Serial.println("Main::setup::begin");
    masterCtrl.init();
    Serial.println("Main::setup::end");
}

void loop()
{
    masterCtrl.update();
}

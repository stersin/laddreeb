#pragma once

#include <Arduino.h>
#include <LaddLib/Component/ButtonComponent.h>
#include <LaddLib/Component/DigitalOutputComponent.h>
#include <LaddLib/Component/DigitalOutputDelayComponent.h>
#include <LaddLib/Component/AxisComponent.h>
#include <LaddLib/Component/AxisComponentTest.h>
#include <LaddLib/Component/AxisComponentTwoEndSwitches.h>
#include <LaddLib/Component/RotatingAxisComponent.h>


#include "LaddLib/Initializable.h"
#include "LaddLib/Updatable.h"
#include "LaddLib/Pausable.h"
#include "LaddLib/Eventable.h"
#include "LaddLib/SerialController.h"

#define NUM_GLASSES_PILES 10

class MasterController:
        public LaddLib::Initializable,
        public LaddLib::Updatable,
        public LaddLib::Pausable {
public:
    enum State {
        NONE,
        INIT,
        IDLE,
        PICK_GLASS,
        PICK_GLASS_RETURN,
        PICK_GLASSES_PILE,
        COUNT_GLASSES_PILES,
        ROTATE_GLASSES_PILE,
        TEST,
        ERROR,
    };

    enum SubState {
        NONE_NONE,
        INIT_0_INIT,
        INIT_1_ZAXIS_TO_MIN,
        INIT_2_DAXIS_TO_MIN,
        
        INIT_2_DAXIS_TO_GRIP_POS,
        INIT_2_DAXIS_TO_MAX,
        INIT_3_YAXIS_TO_MIN,
        INIT_4_RAXIS_TURN,
        
        INIT_5_DAXIS_TO_MIN,

        INIT_ALT_2_YAXIS_TO_MIN,

        INIT_6_TAXIS_TO_MAX,
        INIT_7_OPEN_GRIP1,
        
        PICK_GLASS_0_INIT,
        PICK_GLASS_1_DISTRIBUTE_BEER,
        PICK_GLASS_1_DAXIS_TO_GRIP_POS,
        PICK_GLASS_2_CLOSE_GRIP1,
        PICK_GLASS_3_DISTRIBUTE_BEER,
        PICK_GLASS_4_DAXIS_TO_MAX,
        PICK_GLASS_5_RAXIS_TURN,
        PICK_GLASS_6_DAXIS_TO_MIN,
        PICK_GLASS_7_OPEN_GRIP1,
        PICK_GLASS_8_WAIT_FOR_GLASS_TO_BE_TAKEN,
        
        PICK_GLASS_RETURN_0_INIT,
        PICK_GLASS_RETURN_1_CLOSE_GRIP1,
        PICK_GLASS_RETURN_2_DAXIS_TO_MAX,
        PICK_GLASS_RETURN_3_RAXIS_TURN_REVERSE,
        PICK_GLASS_RETURN_4_DAXIS_TO_GRIP_POS,
        PICK_GLASS_RETURN_5_OPEN_GRIP1,
        PICK_GLASS_RETURN_6_DAXIS_TO_MIN,

        PICK_GLASSES_PILE_0_INIT,
        PICK_GLASSES_PILE_1_OPEN_GRIP1,
        PICK_GLASSES_PILE_2_DAXIS_TO_MAX,
        PICK_GLASSES_PILE_3_ZAXIS_TO_MIN,
        PICK_GLASSES_PILE_4_YAXIS_TO_MAX,
        PICK_GLASSES_PILE_5_FIND_NEXT_GLASSES_PILE,
        PICK_GLASSES_PILE_5_FIND_NEXT_GLASSES_PILE_1_WAIT_FOR_T_AXIS,
        PICK_GLASSES_PILE_6_ZAXIS_TO_MAX,
        PICK_GLASSES_PILE_7_CLOSE_GRIP1,
        PICK_GLASSES_PILE_8_YAXIS_TO_MIN,
        PICK_GLASSES_PILE_9_DAXIS_TO_MIN,
        PICK_GLASSES_PILE_10_OPEN_GRIP1,
        PICK_GLASSES_PILE_11_ZAXIS_TO_MIN,

        ROTATE_GLASSES_PILE_0_INIT,
        ROTATE_GLASSES_PILE_1_ROTATE,

        COUNT_GLASSES_PILES_0_INIT,
        COUNT_GLASSES_PILES_1_Z_AXIS_TO_MIN,
        COUNT_GLASSES_PILES_2_T_AXIS_TO_REF,
        COUNT_GLASSES_PILES_3_ROTATE_AND_COUNT,
        COUNT_GLASSES_PILES_4_ROTATE_AND_COUNT_WAIT_FOR_T_AXIS,

        TEST_INIT_1,
        TEST_D_AXIS_TO_MIN,
        TEST_D_AXIS_TO_MAX,

        TEST_Y_AXIS_TO_MIN,
        TEST_Y_AXIS_TO_MAX,

        TEST_Z_AXIS_TO_MIN,
        TEST_Z_AXIS_TO_MAX,

        TEST_INIT_2,

        TEST_T_AXIS_TO_MIN,
        TEST_T_AXIS_TO_MAX,

        TEST_T_AXIS_TO_INIT,

        TEST_INIT_3,

        TEST_T_AXIS_TO_NEXT,
    };

    MasterController();
protected:
    virtual void onInit() override;
    virtual void onUpdate(unsigned long timeMillis, unsigned long elapsedTimeMillis) override;

    virtual void onPause() override;
    virtual void onResume() override;
    
private:
    State _state;
    SubState _subState;

    LaddLib::SerialController _serialController;
    LaddLib::Component::ButtonComponent _emergSwitch;
    LaddLib::Component::ButtonComponent _processSwitch;
    LaddLib::Component::ButtonComponent _process2Switch;
    LaddLib::Component::ButtonComponent _dBottomSwitch;
    LaddLib::Component::ButtonComponent _dTopSwitch;
    LaddLib::Component::AxisComponentTwoEndSwitches _dAxis;
    LaddLib::Component::DigitalOutputDelayComponent _dDistributeBeerRelay;
    LaddLib::Component::AxisComponentTest _rAxis;
    LaddLib::Component::ButtonComponent _zBottomSwitch;
    LaddLib::Component::ButtonComponent _zTopSwitch;
    LaddLib::Component::AxisComponentTwoEndSwitches _zAxis;
    LaddLib::Component::ButtonComponent _yBottomSwitch;
    LaddLib::Component::ButtonComponent _yTopSwitch;
    LaddLib::Component::AxisComponentTwoEndSwitches _yAxis;
    LaddLib::Component::ButtonComponent _tRevSensor;
    LaddLib::Component::RotatingAxisComponent _tAxis;
    LaddLib::Component::DigitalOutputComponent _grip1Relay;
    LaddLib::Component::ButtonComponent _emptyPileSwitch;
    LaddLib::Component::ButtonComponent _emptyEntrySwitch;
    LaddLib::Component::ButtonComponent _rotatePileSwitch;

    bool _glassesPilesMeasured = false;
    bool _glassesPiles[NUM_GLASSES_PILES] = {false, false, false, false, false, false, false, false, false, false};

    int _currentGlassesPilesIndex = 0;

    uint8_t _distributorPos = 0;
    uint8_t _numToServe = 0;
    uint8_t _remainingToServe = 0;
    uint8_t _qtyToDistribute = 25;

    LaddLib::Eventable::EventHandler _inputNewLineEventHandler;
    
    uint32_t _getUltrasonic1Measure();
    uint32_t _getUltrasonic2Measure();
    uint32_t _getUltrasonic3Measure();

    static void onInputNewLineCallback(LaddLib::SerialController::NewLineEvent& evt,
            LaddLib::Eventable* issuer, void* data);

    void _handleStateInit();
};